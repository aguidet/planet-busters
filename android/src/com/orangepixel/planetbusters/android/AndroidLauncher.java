package com.orangepixel.planetbusters.android;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.orangepixel.planetbusters.myCanvas;

import android.os.Bundle;

public class AndroidLauncher extends AndroidApplication {
	
	static myCanvas startCanvas;
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		
		config.useImmersiveMode=true;
		config.useWakelock=true;
		config.maxSimultaneousSounds=14;

		startCanvas=new myCanvas();

		if (!getPackageManager().hasSystemFeature("android.hardware.touchscreen")) {
			// no touchscreen, change into controller version
			myCanvas.isAndroidTV=true;
		}		
		
		
		initialize(startCanvas, config);	
	}
}
