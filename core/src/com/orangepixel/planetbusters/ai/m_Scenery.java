package com.orangepixel.planetbusters.ai;

import com.orangepixel.planetbusters.Bullets;
import com.orangepixel.planetbusters.FX;
import com.orangepixel.planetbusters.Globals;
import com.orangepixel.planetbusters.Player;
import com.orangepixel.planetbusters.World;
import com.orangepixel.utils.Light;

/**
 * mostly static stuff, sometimes can be destroyed, not required to interact with
 * 
 * @author orangepascal / orangepixel.net
 *
 */
public class m_Scenery {

	
	public final static int		FLOORHOLE = 0,
								METALCRATE = 1,
								PLANETCORE = 2;
	
	
	public final static int propXOffset = 0,
							propYOffset = 1,
							propWidth = 2,
							propHeight = 3;
	
	public final static int[][]	properties = new int[][] {
		{0,53,28,9},		// floorhole
		{0,68,16,13},		// metal crate
		{24,68,16,15},		// planet core
	};	
	
	
	private final static int 	aiIdle=0,
								aiSolid = 1,
								aiCore = 2;
	
	/* 
	 * ================
	 * initialise provided monster into our type
	 * ================
	 */
	
	public final static void init(Monsters e, World myWorld) 
	{
		e.xOffset=properties[e.subType][propXOffset];
		e.yOffset=properties[e.subType][propYOffset];
		e.w=properties[e.subType][propWidth];
		e.h=properties[e.subType][propHeight];
		
		// by default we can't be killed
		e.energy=-1;

		switch (e.subType)
		{
			case FLOORHOLE:
				e.aiState=aiIdle;
			break;
			
			case METALCRATE:
				e.aiState=aiSolid;
				e.energy=32+(myWorld.difficulty*4);
			break;
			
			case PLANETCORE:
				e.aiState=aiCore;
				e.y-=6;
			break;
				
		}
	}

	/*
	 * ================
	 * update specified monster every tick
	 * ================
	 * 	
	 */
	public final static void update(Monsters e, World myWorld, Player myPlayer, boolean hitPlayer) 
	{
		switch (e.aiState)
		{
			case aiIdle:
			break;
			
			case aiSolid:
				myWorld.put(e.x>>4, e.y>>4, World.tMONSTER);
			break;
			
			case aiCore:
				Light.addLight(e.x+8-myWorld.worldOffsetX, e.y+7-myWorld.worldOffsetY, 24, Light.LightType_SphereTense, 100,205,244,200);
				Light.addLight(e.x+8-myWorld.worldOffsetX, e.y+7-myWorld.worldOffsetY, 80, Light.LightType_Sphere, 100,205,244,128);
				FX.addFX(FX.fCOREELECTRO, e.x+3, e.y-4, 0, myWorld);
				
				if (hitPlayer && myPlayer.xSpeed==0 && myPlayer.onGround) myWorld.levelCompleted=true;
			break;
		}
	}
	
	/*
	 * ================
	 * handle being hit by the specified bullet
	 * ================
	 */
	public final static boolean hit(Monsters e,Bullets myBullet, World myWorld) 
	{
		if (e.energy<0) return false;
		
		e.energy-=myBullet.energy;

		if (e.energy<=0) 
		{
			e.died=true;
			myWorld.put(e.x>>4, e.y>>4, World.tEMPTY);
			
			if (Globals.getRandom(100)>80) Monsters.addMonster(Monsters.mPICKUP, e.x, e.y, m_Pickup.CREDITS, myWorld);
			
			FX.addFX(FX.fEXPLOSION, e.x, e.y, 0, myWorld);
			
			for (int i=6; --i>=0;) 
			{
				FX.addFX(FX.fDEBRI, e.x+Globals.getRandom(3), e.y, 0, myWorld);
			}
		}

		return true;
	}
	
	

	/*
	 * ================
	 *  called after map generation is done, so some objects can turn 
	 *  them selves into "solid" wall type objects before the player starts
	 * ================
	 */
	public final static void solidify(Monsters e,World myWorld) 
	{
	}
	

}
