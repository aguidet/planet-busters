package com.orangepixel.planetbusters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.orangepixel.controller.GameInput;
import com.orangepixel.planetbusters.ai.Monsters;
import com.orangepixel.utils.ArcadeCanvas;
import com.orangepixel.utils.Light;

public class myCanvas extends ArcadeCanvas {
	
	
	public final static int[][] windowedModes=new int[][] {
		{1024, 640, 4, 3}, 
		{1024, 768, 4, 3}, 
		{1080, 720, 3, 2}, 
		{1152, 720, 16, 10}, 
		{1280, 720, 16, 9}, 
		{1280, 800, 16, 10}, 
		{9999, 9999, 16, 10}
	};
	
	public static int windowedModeID=1;
	
	// active profile
	PlayerProfile activePlayer;


	
	// used for our splash logo/intro
	static Texture splashImage;
	int splashFrame;
	boolean splashDone;
	int splashAlpha;
	int splashYSpeed;
	int splashY;

	// controller setup
	static int touchSelected;
	static int touchSelectedTx;
	static int touchSelectedTy;	
	static int charSelectDelay;			
	
	// object-pools
//	static FX[] fxList=new FX[2640];
//	static Bullets[] bulletList=new Bullets[128];
//	static Spriter[] spriteList = new Spriter[3200];
	
	static Player	myPlayer=new Player();
	static World	myWorld=new World();
	
	
	// interface bits and bobs
	int		levelTitleCountdown;
	int		menuSelectedItem;
	int		menuSelectedItem2;
	int[]	CompletedMissionsAlpha= new int[4];
	int[]	CompletedMissionsAlphaTarget = new int[4];
	
	
	// global used for drawing, calculating, etc
	// bit dirty hack, but I got used to these variables always being "there"
	static int tx;
	static int ty;
	static int tx2;
	static int ty2;
	static int tile;

	
	


	// Main game initialisation
	public void init() {
		// create our splash image (Orangepixel logo)
		splashImage=new Texture(Gdx.files.internal("spl2.png"), true);
		splashFrame=0;
		splashDone=false;
		splashAlpha=0;
		splashY=0;
		splashYSpeed=-8;

		// initialise player profile, for saving/loading settings and progress
		activePlayer=new PlayerProfile("PLANETBUSTER");
		activePlayer.loadSettings();

		
		// keyboard settings from preferences or defaults
		for (int i=0; i<=10; i++) {
			if (activePlayer.keyboardSettings[i]<0) activePlayer.keyboardSettings[i]=GUI.keyboardConfigDefault[i];
			GUI.keyboardConfig[i]=activePlayer.keyboardSettings[i];
		}


		// init the sound engine
		Audio.initSounds();
		Audio.MusicVolume=activePlayer.musicVolume;
		Audio.SoundVolume=activePlayer.soundVolume;
		
		GUI.initGui("uipcfont.png");


		// generate our entity objects
		Monsters.initMonsters();
		Bullets.initBullets();
		FX.initFX();

		
		
		// world graphics, freed and reloaded at loadWorld
		sprites[1]=new Texture(Gdx.files.internal("m01.png"), true);
		sprites[2]=new Texture(Gdx.files.internal("t01.png"), true);
		
		GameInput.initControllers();
		
		// set correct resolution
		windowedModeID=activePlayer.storedWindowedModeID;
		if (windowedModeID < 0) {
			// first start? set to 1080x720 and windowed
			windowedModeID=2;
			activePlayer.storedWindowedModeID=windowedModeID;
			activePlayer.saveSettings();
		}
		setDisplayMode(windowedModes[windowedModeID][0], windowedModes[windowedModeID][1], activePlayer.useFullscreen);

		
		// Light system
		Light.initLights();
		Light.isLightRendering=false;
		// set the "light" sprite
		Light.lightSprite=new Texture(Gdx.files.internal("lights.png"), true);
		
		// start our Splash, including a nice sound
		worldTicks=0;
		Audio.playSound(Audio.FX_SPLASH);
		GameState=INSPLASH;
	}
	
	
	
	/* ===============
	 * All the gameloop code is handled here, this is called every tick
	 * ===============
	 */
	public final void GameLoop() {

		switch (GameState) 
		{
			case ININIT:
				init();
				break;

				
				
				
				
			case INSPLASH:
				Render.drawPaint(255, 255, 255, 255);
				Render.setAlpha(splashAlpha);
				
				if (!splashDone || splashAlpha < 255) 
				{
					splashAlpha+=32;
					if (splashAlpha > 255)
						splashAlpha=255;
				}

				if (!splashDone && worldTicks % 2 == 0) 
				{
					splashFrame+=16;
					if (splashFrame == 96) 
					{
						splashFrame=0;
						splashDone=true;
					}
				}

				// jump
				if (splashYSpeed < 6 && worldTicks % 2 == 0) splashYSpeed++;
				splashY+=splashYSpeed;
				if (splashY >= 0) 
				{
					splashY=0;
					splashYSpeed=-(splashYSpeed >> 1);
				}

				// center on screen
				tx=(Render.width >> 1) - 36;
				ty=((Render.height >> 1) - 48) + splashY;
				// render pixel
				Render.dest.set(tx, ty, tx + 72, ty + 72);
				Render.src.set(splashFrame, 0, splashFrame + 16, 16);
				Render.drawBitmap(splashImage);

				// render name
				tx=(Render.width >> 1) - 61;
				ty=(Render.height >> 1) + 30;
				Render.dest.set(tx, ty, tx + 122, ty + 26);
				Render.src.set(0, 16, 122, 42);
				Render.drawBitmap(splashImage);

				
				if (worldTicks > 48 && splashDone) 
				{
					// initialise social hooks (steam, googleplay, gamecenter)
					// set this in the specific platform launcher activities
					if (mySocial != null) 
					{
						mySocial.initSocial();
						mySocial.loginSocial();
					}

					// set touch controls to defaults
					activePlayer.resetControls();

					// using day,month, and year values as seeds you can do daily challenges 
					Globals.fillRandomTable( Globals.getRandomNonSeeded(9999), Globals.getRandomNonSeeded(9999), Globals.getRandomNonSeeded(9999));
					
					initNewGame();
				}
				break;
				
				
			
				

			case INITMAP:
				Monsters.killAll();
				FX.killAll();
				Bullets.killAll();

				myWorld.initWorld();
				myWorld.generateWorld();
				myPlayer.init( 48, Render.height-32);

				// load correct tiles
				switch (myWorld.Spriteset) {
					case 1:
						sprites[2]=new Texture(Gdx.files.internal("t01.png"), true);
						setAmbientLight(0.1f, 0.1f, 0.2f, 1f);
	
					break;
					
					case 2:
						sprites[2]=new Texture(Gdx.files.internal("t02.png"), true);
						setAmbientLight(0.2f, 0.1f, 0.28f, 1f);
					break;
	
					case 3:
						sprites[2]=new Texture(Gdx.files.internal("t03.png"), true);
						setAmbientLight(0.1f, 0.3f, 0.18f, 1f);
					break;
				}				

				levelTitleCountdown=256;
				
				Light.isLightRendering=true;
				GameState=INGAME;
			break;
				
				
				
			case INGAME:
				Light.clearLights();
				handleInput();

				
				myPlayer.update(myWorld);
				myWorld.update();
				myWorld.handleCamera(myPlayer);
				
				// add "planet side sun light"
				Light.addLight((Render.width>>1)-myWorld.worldOffsetX,(-64)-myWorld.worldOffsetY,256,Light.LightType_Sphere,29,175,243,255);
				
				
				Monsters.updateMonsters(myWorld, myPlayer);
				Bullets.updateBullets(myWorld, myPlayer);
				FX.updateFX(myWorld,myPlayer);
				
				
				renderScene();
				
				
				if (myWorld.levelCompleted) 
				{
					// choose next mission!
					menuSelectedItem=0;
					Light.isLightRendering=false;
					
					Monsters.killAll();
					FX.killAll();
					Bullets.killAll();
					
					myPlayer.visible=false;
					
					
					myWorld.level++;
					if (myWorld.level==4) 
					{
						myWorld.world++;
						myWorld.createGalaxy();
						myWorld.level=0;
					}
					GameState=INITMAP;
					
				}
			break;
		}
	}
	
	
	
	// Render everything uneffected by lights (statusbar, dialogs, messages, etc)
	/* ===============
	 * This is called after the lights have been rendered
	 * things uneffected by lighting should be rendered here (statusbar, hints, text, etc)
	 * ===============
	 */
	public final void GameLoopPostLights() {
		switch (GameState) 
		{
			case INGAME:

				// render topscene (on top of lights)
				// render solid tiles ("underworld")
				tx=-myWorld.worldOffsetX;
				for (int x=0; x < World.tileMapW; x++) 
				{
					if (tx>-16 && tx<Render.width) 
					{
						ty=48-myWorld.worldOffsetY;
						for (int y=3; y < World.tileMapH; y++) 
						{
							if (ty>-16 && ty<Render.height) 
							{
								if (myWorld.getTile(x,y)==World.tSOLID) 
								{
									Render.dest.set(tx,ty,tx+16,ty+16);
									Render.src.set(0,96,16,112);
									Render.drawBitmap(sprites[1]);
								}
							}
							ty+=16;
						}
					}
					tx+=16;
				}
				
				renderFX(9);
				renderStatusbar();
				
				if (GameInput.controller1.isTouchscreen)
				{
					renderTouchControlls();
				}
				
				
				// render level title
				if (levelTitleCountdown>0) 
				{
					levelTitleCountdown--;
					GUI.renderText(Globals.galaxyNamePre[myWorld.worldPre]+" "+Globals.galaxyNamePost[myWorld.worldPost],0,-1,32,116,0,2);
					
					tx=(Render.width>>1)-59;
					ty=40;
					Render.dest.set(tx,ty,tx+117,ty+1);
					Render.src.set(0,137,117,138);
					Render.drawBitmap(sprites[1]);
					GUI.renderText( Mission.getName(myWorld.level) , 0, -1, 42, 320, 0, 0);
					GUI.renderText("("+myWorld.world+"."+(myWorld.level+1)+")",0,-1,52,320,0,0);
				}
			break;
			
			
		}
	}
	
	
	/* ===============
	 * check the specific controllers for user input
	 * ===============
	 */
	public final void handleInput() {
		myPlayer.resetInput();
		
		if (GameInput.controller1.isKeyboard || GameInput.controller1.isGamepad)
		{
			if (GameInput.controller1.leftPressed) {
				myPlayer.leftPressed=true;
			}
			
			if (GameInput.controller1.rightPressed) {
				myPlayer.rightPressed=true;
			}
			
			if (GameInput.controller1.upPressed) {
				myPlayer.upPressed=true;
			}
			
			if (GameInput.controller1.BUTTON_X) {
				myPlayer.actionPressed=true;
			}
			
			// TEST
			if (GameInput.controller1.backPressed && !GameInput.controller1.backLocked) {
				initNewGame();
			}
			
		}
		
		
		float tmpX;
		float tmpY;
		
		if (GameInput.controller1.isTouchscreen)
		{
			for (int button=4; --button>=0;) {
				tx = activePlayer.stickX[button];
				ty = activePlayer.stickY[button];
				
				tx-=4;
				ty-=4;
				if (ty<0) ty=0;
				if (tx<0) tx=0;
			
				for (int j = GameInput.mTouchX.length; --j >= 0;) {

					tmpX = GameInput.mTouchX[j];
					tmpY = GameInput.mTouchY[j];
					

					if (tmpX>=tx && tmpX<=tx+40 && tmpY>=ty && tmpY<=ty+40) {
						switch (button) {
							case 0: 
								myPlayer.leftPressed=true;
							break;
							case 1: 
								myPlayer.rightPressed=true;
							break;
							case 2: 
								myPlayer.upPressed=true;
							break;
							case 3: 
								myPlayer.actionPressed=true;
							break;
						}
						GameInput.touchReleased=false;
					}
				}
				
			}		
			
			if (GameInput.controller1.backPressed && !GameInput.controller1.backLocked) {
				Gdx.app.exit();
			}
			
		}
		
	}
	
	
	
	
	
	
	// [ INIT CODE! ]----------------------------------------------------------------------------

	/* ===============
	 * initialise a new game, resetting the world, etc
	 * ===============
	 */
	public final void initNewGame() {
		myPlayer.newGameInit();
		myWorld.initNewGame();
		
		myWorld.level=0;
		GameState=INITMAP;
	}
	
	
	
	
	
	
	// [ RENDERING STUFF! ]----------------------------------------------------------------------------
	/* ===============
	 * renders a complete scene, world graphics, entities, etc
	 * ===============
	 */
	public final void renderScene() {
		Render.drawPaint(255, 0, 0, 0);
		
		// render planet (top-part)
		tx=-(128+(myWorld.worldOffsetX%96));
		ty=(-44)-myWorld.worldOffsetY;
		while (tx<Render.width) 
		{
			Render.dest.set(tx,ty,tx+96,ty+96);
			Render.src.set(0,32,96,128);
			Render.drawBitmap(sprites[2]);
			tx+=96;
		}

		// render background tiles ("underworld")
		tx=-myWorld.worldOffsetX;
		for (int x=0; x < World.tileMapW; x++) 
		{
			if (tx>-16 && tx<Render.width) 
			{
				ty=48-myWorld.worldOffsetY;
				for (int y=3; y < World.tileMapH; y++) 
				{
					if (ty>-16 && ty<Render.height) 
					{
						if (myWorld.getTile(x,y)!=World.tSOLID) 
						{
							tile=myWorld.renderMap[x+(y*World.tileMapW)];

							Render.dest.set(tx, ty, tx + 16, ty + 16);
							Render.src.set(((tile & 7) << 4), ((tile >> 3) << 4), ((tile & 7) << 4) + 16, ((tile >> 3) << 4) + 16);
							Render.drawBitmap(sprites[2]);
						}
					}
					ty+=16;
				}
			}
			tx+=16;
		}		
		
		
		renderMonsters(1);
		renderPlayer(myPlayer);
		renderBullets(1);
		renderFX(1);
		
		// render planet (bottom-part) hanging over the first row of the underworld images
		tx=-(128+(myWorld.worldOffsetX%96));
		ty=(-44)-myWorld.worldOffsetY;
		while (tx<Render.width) 
		{
			Render.dest.set(tx,ty+72,tx+96,ty+96);
			Render.src.set(0,104,96,128);
			Render.drawBitmap(sprites[2]);
			tx+=96;
		}		
		
		
	}
	
	
	/* ===============
	 * render a player object, including his weapon
	 * ===============
	 */
	public final void renderPlayer( Player tmpPlayer ) {
		if (!tmpPlayer.visible) return;
		
		tx=tmpPlayer.x-myWorld.worldOffsetX;
		ty=tmpPlayer.y-myWorld.worldOffsetY;
		
		
		// render weapon? (on our back/behind player when not shooting)
		if (tmpPlayer.actionDelay==0) renderWeapon(tmpPlayer);

		// player sprite
		Render.dest.set(tx,ty,tx+10,ty+10);
		Render.src.set(tmpPlayer.xOffset, tmpPlayer.yOffset, tmpPlayer.xOffset+tmpPlayer.w, tmpPlayer.yOffset+tmpPlayer.h);
		Render.drawBitmap(sprites[1]);
		
		// render weapon?
		if (tmpPlayer.actionDelay>0) renderWeapon(tmpPlayer);
	}

	
	/* ===============
	 * render monster entities
	 * you can use myRenderPassID to just render 
	 * those entities with renderPass set to that value
	 * ===============
	 */
	public final void renderMonsters(int myRenderPassID) {
		int i=0;
		int tx;
		int ty;
		
		Monsters tmpMonster;
		
		while (i<Monsters.monsterList.length) 
		{
			tmpMonster=Monsters.monsterList[i];
			
			if (!tmpMonster.deleted 
					&& !tmpMonster.died 
					&& tmpMonster.visible 
					&& tmpMonster.renderPass==myRenderPassID) 
			{
					tx=tmpMonster.x-myWorld.worldOffsetX;
					ty=tmpMonster.y-myWorld.worldOffsetY;
					
					Render.dest.set(tx,ty,tx+tmpMonster.w, ty+tmpMonster.h);
					Render.src.set(tmpMonster.xOffset, tmpMonster.yOffset, tmpMonster.xOffset+tmpMonster.w, tmpMonster.yOffset+tmpMonster.h);
					Render.drawBitmap(sprites[1]);
					
			}
			i++;
		}
	}
	
	
	/* ===============
	 * render bullets entities
	 * you can use myRenderPassID to just render 
	 * those entities with renderPass set to that value
	 * ===============
	 */
	public final void renderBullets(int myRenderPassID) {
		int i=0;
		int tx;
		int ty;
		
		Bullets tmpBullet;
		
		while (i<Bullets.bulletList.length) 
		{
			tmpBullet=Bullets.bulletList[i];
			
			if (!tmpBullet.deleted 
					&& !tmpBullet.died 
					&& tmpBullet.visible 
					&& tmpBullet.renderPass==myRenderPassID) 
			{
					tx=tmpBullet.x-myWorld.worldOffsetX;
					ty=tmpBullet.y-myWorld.worldOffsetY;
					
					Render.dest.set(tx,ty,tx+tmpBullet.w, ty+tmpBullet.h);
					Render.src.set(tmpBullet.xOffset, tmpBullet.yOffset, tmpBullet.xOffset+tmpBullet.w, tmpBullet.yOffset+tmpBullet.h);
					Render.drawBitmap(sprites[1]);
					
			}
			i++;
		}
	}	
	
	
	/* ===============
	 * render FX entities
	 * you can use myRenderPassID to just render 
	 * those entities with renderPass set to that value
	 * ===============
	 */
	public final void renderFX(int myRenderPassID) {
		int i=0;
		int tx;
		int ty;
		
		FX tmpFX;
		
		while (i<FX.fxList.length) 
		{
			tmpFX=FX.fxList[i];
			
			if (!tmpFX.deleted 
					&& !tmpFX.died 
					&& tmpFX.visible 
					&& tmpFX.renderPass==myRenderPassID) 
			{
				tx=tmpFX.x-myWorld.worldOffsetX;
				ty=tmpFX.y-myWorld.worldOffsetY;
				
				Render.setAlpha(tmpFX.alpha);
				if (tmpFX.myType==FX.fSPEECH) {
					GUI.renderText(Globals.dudeQuotes[tmpFX.subType],0,tx,ty,180,0,0);
				} else if (tmpFX.myType==FX.fSCOREPLUME) {
					GUI.renderText(Integer.toString(tmpFX.subType),0,tx,ty,180,0,6);
				} else {
					Render.dest.set(tx,ty,tx+tmpFX.w, ty+tmpFX.h);
					Render.src.set(tmpFX.xOffset, tmpFX.yOffset, tmpFX.xOffset+tmpFX.w, tmpFX.yOffset+tmpFX.h);
					Render.drawBitmap(sprites[1]);
				}
			}
			i++;
		}
		
		Render.setAlpha(255);
	}		
	
	
	/* ===============
	 * Render the weapon of the specified player
	 * ===============
	 */
	public final void renderWeapon(Player tmpPlayer) { 
		int weaponYOffset=0;
		if (tmpPlayer.xOffset==10 || tmpPlayer.xOffset==40) weaponYOffset=1;

		int myX=tmpPlayer.x-myWorld.worldOffsetX+5;
		int myY=tmpPlayer.y-myWorld.worldOffsetY+5+weaponYOffset;
		int weaponID=0;
		
		if (tmpPlayer.myDirection==Globals.RIGHT) 
		{
			if (tmpPlayer.actionDelay>0) 
			{
				myX-=1;
				myY-=2;
				
				Render.dest.set(myX,myY,myX+Globals.weaponValues[weaponID][2],myY+Globals.weaponValues[weaponID][3]);
				Render.src.set( Globals.weaponValues[weaponID][0], Globals.weaponValues[weaponID][1],
						 Globals.weaponValues[weaponID][0]+Globals.weaponValues[weaponID][2], Globals.weaponValues[weaponID][1]+Globals.weaponValues[weaponID][3]);
				Render.drawBitmap(sprites[1]);
			} 
			else 
			{
				// weapon on our back
				myX-=12;
				myY-=6;
				Render.dest.set(myX,myY,myX+Globals.weaponValues[weaponID][2],myY+Globals.weaponValues[weaponID][3]);
				Render.src.set( Globals.weaponValues[weaponID][0], Globals.weaponValues[weaponID][1],
						 Globals.weaponValues[weaponID][0]+Globals.weaponValues[weaponID][2], Globals.weaponValues[weaponID][1]+Globals.weaponValues[weaponID][3]);
				Render.drawBitmapRotated(sprites[1], -120);
			}
		} 
		else 
		{
			if (tmpPlayer.actionDelay>0) 
			{
				myX+=1;
				myY-=2;
				
				Render.dest.set(myX-Globals.weaponValues[weaponID][2],myY,myX,myY+Globals.weaponValues[weaponID][3]);
				Render.src.set( Globals.weaponValues[weaponID][0],  (Globals.weaponValues[weaponID][1]+Globals.weaponValues[weaponID][3]),
						 Globals.weaponValues[weaponID][0]+Globals.weaponValues[weaponID][2], 
						 (Globals.weaponValues[weaponID][1]+Globals.weaponValues[weaponID][3])+Globals.weaponValues[weaponID][3]);
				Render.drawBitmap(sprites[1]);
			} 
			else 
			{
				myX+=12;
				myY-=6;
				Render.dest.set(myX-Globals.weaponValues[weaponID][2],myY,myX,myY+Globals.weaponValues[weaponID][3]);
				Render.src.set( Globals.weaponValues[weaponID][0],  (Globals.weaponValues[weaponID][1]+Globals.weaponValues[weaponID][3]),
						 Globals.weaponValues[weaponID][0]+Globals.weaponValues[weaponID][2], 
						 (Globals.weaponValues[weaponID][1]+Globals.weaponValues[weaponID][3])+Globals.weaponValues[weaponID][3]);
				Render.drawBitmapRotated(sprites[1], 120);
			}
			
		}

	}
	
	/* ===============
	 * render the statusbar (health, energy)
	 * ===============
	 */
	public final void renderStatusbar() {
		// render score
//		GUI.renderText(String.format("%08d", myPlayer.score),0, -1,16,80,0,0);
		
		// money collected?
		renderCredits();
		
		// render heart above player (blinking if just hit)
		if (myPlayer.visible && (myPlayer.healthDelay==0 || myPlayer.healthDelay%4<2) ) 
		{
			tx=(myPlayer.x-myWorld.worldOffsetX);
			ty=(myPlayer.y-myWorld.worldOffsetY)-9+(myPlayer.healthDelay>>2);
			Render.dest.set(tx,ty,tx+7,ty+7);
			Render.src.set(17,96, 24,103);
			Render.drawBitmap(sprites[1]);
			// render digit value 
			tx+=9;
			Render.dest.set(tx,ty,tx+5,ty+7);
			Render.src.set(156+(myPlayer.health*6), 0,161+(myPlayer.health*6), 7);
			Render.drawBitmap(GUI.guiImage);
		}
	}
	
	
	/* ===============
	 * render touch screen controller
	 * ===============
	 */
	
	public final void renderTouchControlls() {
		for (int button=4; --button>=0;) {
			tx = activePlayer.stickX[button];
			ty = activePlayer.stickY[button];
			

				switch (button) {
					case 0 : // left
						Render.setAlpha(120);
						Render.dest.set(tx, ty, tx + 30, ty + 30);
						Render.src.set(0, 98, 30, 128);
						Render.drawBitmap(GUI.guiImage);

						Render.setAlpha(255);
						Render.dest.set(tx+8,ty+8,tx+18,ty+22);
						Render.src.set(104,98,114,112);
						Render.drawBitmap(GUI.guiImage);
					break;
					
					case 1 : // right
						Render.setAlpha(120);
						Render.dest.set(tx, ty, tx + 30, ty + 30);
						Render.src.set(30, 98, 60, 128);
						Render.drawBitmap(GUI.guiImage);
						
						Render.setAlpha(255);
						Render.dest.set(tx+12,ty+8,tx+22,ty+22);
						Render.src.set(114,98,124,112);
						Render.drawBitmap(GUI.guiImage);
					break;

					case 2 : // up
						Render.setAlpha(120);
						Render.dest.set(tx, ty, tx + 30, ty + 30);
						Render.src.set(60, 98, 90, 128);
						Render.drawBitmap(GUI.guiImage);
						
						Render.setAlpha(255);
						Render.dest.set(tx+8,ty+10,tx+22,ty+20);
						Render.src.set(124,98,138,108);
						Render.drawBitmap(GUI.guiImage);
					break;
					
					case 3 : // fire
						Render.setAlpha(120);
						Render.dest.set(tx, ty, tx + 30, ty + 30);
						Render.src.set(60, 98, 90, 128);
						Render.drawBitmap(GUI.guiImage);
						
						Render.setAlpha(255);
						Render.dest.set(tx+8,ty+8,tx+22,ty+22);
						Render.src.set(124,108,138,122);
						Render.drawBitmap(GUI.guiImage);
					break;
				}
		}
		

	}

	/* ===============
	 * render the player's collected credits
	 * ===============
	 */
	public final void renderCredits() {
		tx=Render.width-64;
		GUI.renderText( Integer.toString(myPlayer.creditsCollected), 0, tx,16,64,0,0);
		tx-=8;
		Render.dest.set(tx,16,tx+7,23);
		Render.src.set(17,68,24,75);
		Render.drawBitmap(sprites[1]);
	}
	
	
	
	
}
