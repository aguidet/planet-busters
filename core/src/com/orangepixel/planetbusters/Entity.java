package com.orangepixel.planetbusters;

/**
 * Base Entity class, monster, bullets, fx ,players, all extend from this
 * 
 * @author orangepascal / orangepixel.net
 *
 */
public class Entity {

	public int		x;
	public int		y;
	public int		xOffset;
	public int		yOffset;
	public int		w;
	public int		h;
	public int		rotation;
	public int		alpha;
	
	public boolean	died;
	public boolean	deleted;
	
}
