package com.orangepixel.planetbusters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;

/**
 * Graphical interface stuff, like rendering text, buttons, menu bars, etc
 * 
 * @author orangepascal / orangepixel.net
 *
 */
public class GUI {

	// DEFAULT KEYBOARD MAPPING
	public final static int	
						keyboardConfig_left=0,
						keyboardConfig_right = 1,
						keyboardConfig_up = 2,
						keyboardConfig_down = 3,
						keyboardConfig_select = 4,
						keyboardConfig_cancel = 5,
						keyboardConfig_inventory = 6,
						keyboardConfig_map = 7,
						keyboardConfig_weapon1 = 8,
						keyboardConfig_weapon2 = 9,
						keyboardConfig_weapon3 = 10,
						keyboardConfig_weapon4 = 11; 
						
	public static String[] keyboardConfigNames = new String[] {
			"left",
			"right",
			"up",
			"down",
			"select",
			"cancel",
			"char swap",
			"map",
			"weapon 1",
			"weapon 2",
			"weapon 3",
			"melee"
	};
	
	public static int[]	keyboardConfig = new int[] {
			Keys.LEFT, 	
			Keys.RIGHT,
			Keys.UP,
			Keys.DOWN,
			Keys.X,
			Keys.ESCAPE,
			Keys.Z,
			Keys.M,
			Keys.NUM_1,
			Keys.NUM_2,
			Keys.NUM_3,
			Keys.NUM_4
	};
	
	public static int[]	keyboardConfigDefault = new int[] {
			Keys.LEFT, 	
			Keys.RIGHT,
			Keys.UP,
			Keys.DOWN,
			Keys.X,
			Keys.ESCAPE,
			Keys.Z,
			Keys.M,
			Keys.NUM_1,
			Keys.NUM_2,
			Keys.NUM_3,
			Keys.NUM_4
	};	
	
	
	
	public static Texture 	guiImage;
	public static int		menuSelectedItem;
	public static int		menuSelectedItem2;

	
	
	
	public final static void initGui(String fileName) {
		guiImage=new Texture(Gdx.files.internal(fileName), true);
	}
	
	
	
	
	
	
	
	
	public final static int renderButton(int myx, int myy, int keyboardIDX, boolean renderBackground) {
		if (guiImage==null) {
			Globals.debug("gui Image not initialised, make sure to call initGui()");
			return -1;
		}
		
		// 3letter buttons
		if (keyboardConfig[keyboardIDX]==Keys.ESCAPE || keyboardConfig[keyboardIDX]==Keys.TAB) {
			if (renderBackground) {
				Render.dest.set(myx, myy - 1, myx + 18, myy + 8);
				Render.src.set(160,23,178,32);
				Render.drawBitmap(guiImage);
			}
			
			switch (keyboardConfig[keyboardIDX]) {
				case Keys.ESCAPE:
					Render.dest.set(myx, myy - 1, myx + 18, myy + 8);
					Render.src.set(60,23,78,32);
					Render.drawBitmap(guiImage);
				break;

				case Keys.TAB:
					Render.dest.set(myx, myy - 1, myx + 18, myy + 8);
					Render.src.set(100,23,118,32);
					Render.drawBitmap(guiImage);
				break;
				
				case Keys.ENTER:
										
				break;
			}
			// warning: increase global value TX
			return 9;
		} else if (keyboardConfig[keyboardIDX]==Keys.ENTER) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+1, myy+1, myx+7, myy+8);
			Render.src.set(152, 25, 158, 32);
			Render.drawBitmap(guiImage);	
		} else if (keyboardConfig[keyboardIDX]==Keys.LEFT) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+3, myy+1, myx+7, myy+6);
			Render.src.set(129, 25, 133, 30);
			Render.drawBitmap(guiImage);				
		} else if (keyboardConfig[keyboardIDX]==Keys.RIGHT) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+3, myy+1, myx+7, myy+6);
			Render.src.set(135, 25, 139, 30);
			Render.drawBitmap(guiImage);				
		} else if (keyboardConfig[keyboardIDX]==Keys.UP) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+2, myy+2, myx+7, myy+6);
			Render.src.set(179, 23, 184, 27);
			Render.drawBitmap(guiImage);				
		} else if (keyboardConfig[keyboardIDX]==Keys.DOWN) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+2, myy+2, myx+7, myy+6);
			Render.src.set(179, 28, 184, 32);
			Render.drawBitmap(guiImage);		
		} else if (keyboardConfig[keyboardIDX]>=Keys.F1 && keyboardConfig[keyboardIDX]<=Keys.F12) {
			// F
			Render.dest.set(myx+2, myy+1, myx+7, myy+8);
			Render.src.set(30,16, 35,16+7);
			Render.drawBitmap(guiImage);
			
			// #
			int CharValue=keyboardConfig[keyboardIDX]-Keys.F1;
			Render.dest.set(myx+8, myy+1, myx+14, myy+8);
			Render.src.set(162+(CharValue*6), 16, 167+(CharValue*6), 16+7);
			Render.drawBitmap(guiImage);
			return 9;
			
		} else {
			// key-name
			int CharValue=(Keys.toString( keyboardConfig[keyboardIDX] ).charAt(0));
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			
			if (CharValue>=65 && CharValue<=91) {
				CharValue-=65;
				// letters
				Render.dest.set(myx+2, myy+1, myx+7, myy+8);
				Render.src.set(CharValue*6, 16, (CharValue*6)+5, 16+7);
				Render.drawBitmap(guiImage);
			} else if (CharValue>=48 && CharValue<=58) {
				// digits
				CharValue-=48;
				Render.dest.set(myx+2, myy+1, myx+7, myy+8);
				Render.src.set(156+(CharValue*6), 16, 161+(CharValue*6), 16+7);
				Render.drawBitmap(guiImage);
			}
		}
		
		return 0;
	}	
	
	public final static void renderText(String myText, int startCharacter, int myX, int myY, int myWidth, int myLineCount, int fontID) {
		if (guiImage==null) {
			Globals.debug("gui Image not initialised, make sure to call initGui()");
			return;
		}

		String convertedText=myText.toUpperCase();
		char[] myChars=convertedText.toCharArray();

		int tx=myX;
		int ty=myY;
		int ty2;
		int CharValue;
		int tLineCount=0;
		int wordLength;
		int wordID;

		int yOffset=0;
		int fontWidth=5;
		int fontWidthSprite=6;
		yOffset=(fontID * 8);
		if (fontID==3) {
			yOffset=32;
			fontWidth=7;
			fontWidthSprite=7;
		}

		// center text?
		if (tx == -1) {
			if (fontID>1) {
				// count "Spaces"
				ty2=0;
				for (int i=0; i < myChars.length; i++) {
					if ((int) myChars[i] == 32) ty2++;
				}

				tx=(Render.width >> 1) - ( ((myChars.length-ty2) * fontWidth) >>1);
				tx-=ty2;
			} else {
				tx=(Render.width >> 1) - (myChars.length * 2); // *2 = half
																// width of 1
																// character
			}

			if (myText.indexOf("~") > 0)
				tx+=4;

			myX=tx;
			// Gdx.app.log("opdebug","render text, displayW:"+displayW+"  / tx:"+tx);
		}

		int i=startCharacter;

		while (i < myChars.length) {
			CharValue=(int) myChars[i];

			// detect if next word fits in this space
			wordLength=0;
			wordID=i;
			while ((int) myChars[wordID] != 32 && wordID < myChars.length - 1) {
				wordLength++;
				wordID++;
			}

			if (tx + (wordLength*fontWidth) > myX + myWidth) {
				tx=myX;
				ty+=7;
				tLineCount++;
				if (tLineCount >= myLineCount) {
					return;
				}
			}

			switch (CharValue) {
				case 32:// space
					tx+=3;
					break;

				case 33: // !
					tx++;
					Render.dest.set(tx, ty, tx + 3, ty + 7);
					Render.src.set(247, yOffset, 250, yOffset + 7);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 39: // '
					tx++;
					Render.dest.set(tx, ty, tx + 4, ty + 4);
					Render.src.set(236, yOffset, 240, yOffset + 4);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 40: // (
					tx++;
					Render.dest.set(tx, ty, tx + 2, ty + 6);
					Render.src.set(223, yOffset, 225, yOffset + 6);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 41: // )
					tx++;
					Render.dest.set(tx, ty, tx + 2, ty + 6);
					Render.src.set(228, yOffset, 230, yOffset + 6);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 43: // +
					tx++;
					Render.dest.set(tx, ty + 2, tx + 3, ty + 5);
					Render.src.set(79, 25, 82, 28);
					Render.drawBitmap(guiImage);
					tx+=4;
					break;

				case 45: // -
					tx++;
					Render.dest.set(tx, ty + 3, tx + 3, ty + 4);
					Render.src.set(79, 26, 82, 27);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 46: // .
					tx++;
					Render.dest.set(tx, ty, tx + 3, ty + 7);
					Render.src.set(232, yOffset, 235, yOffset + 7);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 47: // /
					tx++;
					Render.dest.set(tx, ty + 1, tx + 3, ty + 6);
					Render.src.set(84, 24, 87, 29);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 58: // :
					tx++;
					Render.dest.set(tx, ty, tx + 3, ty + 7);
					Render.src.set(250, yOffset, 253, yOffset + 7);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 60: // <
					Render.dest.set(tx, ty, tx + 4, ty + 7);
					Render.src.set(129, 24, 133, 31);
					Render.drawBitmap(guiImage);
					tx+=5;
					break;

				case 62: // >
					Render.dest.set(tx, ty, tx + 4, ty + 7);
					Render.src.set(135, 24, 139, 31);
					Render.drawBitmap(guiImage);
					tx+=5;
					break;

				case 63: // ?
					tx++;
					Render.dest.set(tx, ty, tx + 5, ty + 7);
					Render.src.set(241, yOffset, 246, yOffset + 7);
					Render.drawBitmap(guiImage);
					tx+=5;
					break;

				case 124: // | used as line-break
					tx=myX;
					ty+=7;
					tLineCount++;
					if (tLineCount >= myLineCount) {
						return;
					}
					break;

				case 126: // used for button/key images
					i++;
					CharValue=(int) myChars[i];
					switch (CharValue - 48) {
						case 0: // gamepad-A
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(0, 23, 9, 32);
							Render.drawBitmap(guiImage);
							break;

						case 1: // gamepad-B
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(10, 23, 19, 32);
							Render.drawBitmap(guiImage);
							break;

						case 2: // gamepad-X
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(20, 23, 29, 32);
							Render.drawBitmap(guiImage);
							break;

						case 3: // gamepad-Y
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(30, 23, 39, 32);
							Render.drawBitmap(guiImage);
							break;

						case 4: // keyboard-X
							tx+=renderButton(tx,ty,keyboardConfig_select,true);
							break;

						case 5: // keyboard-I
							tx+=renderButton(tx,ty,keyboardConfig_inventory,true);
							break;

						case 6: // keyboard-ESC
							tx+=renderButton(tx,ty,keyboardConfig_cancel,true);
							break;

						case 7: // keyboard-M
							tx+=renderButton(tx,ty,keyboardConfig_map,true);
							break;

						case 8: // keyboard-TAB
							tx+=renderButton(tx,ty,keyboardConfig_map,true);
							break;

						case 9: // xbox |> start
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(118, 23, 127, 31);
							Render.drawBitmap(guiImage);
							break;

						case 20: // ~d = DPAD / arrow
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(141, 23, 150, 32);
							Render.drawBitmap(guiImage);
							break;

					}
					tx+=10;
					break;

				default:
					if (CharValue > 64 && CharValue < 91) {
						// letters
						CharValue-=65;

						if (CharValue==12 || CharValue==22) tx++;
						Render.dest.set(tx, ty, tx+fontWidth, ty + 7);
						Render.src.set(CharValue*fontWidthSprite, yOffset, (CharValue*fontWidthSprite)+fontWidth, yOffset + 7);
						Render.drawBitmap(guiImage);
						if (CharValue==12 || CharValue==22) tx++;

						if (fontID==3) tx+=fontWidth;
						else if (fontID==2) tx+=6;
						else tx+=4;
					} else if (CharValue > 47 && CharValue < 58) {
						// numbers
						CharValue-=48;
						
						Render.dest.set(tx, ty, tx+fontWidth, ty + 7);
						Render.src.set(156 + (CharValue*fontWidthSprite), yOffset, 156+(CharValue*fontWidthSprite)+fontWidth, yOffset + 7);
						Render.drawBitmap(guiImage);

						if (fontID==3) tx+=fontWidth;
						else if (fontID == 2) tx+=6;
						else tx+=4;
					} else {
						// unidentified char
					}
					break;
			}

			i++;
		}
	}

	
	
	
	
	// Renders a vertical "interface" bar
	public final static void renderMenuBar(Texture image, int myX, int myY, int myW, boolean isSelected) {
		if (isSelected) {
			Render.setAlpha(255);
			Render.dest.set(myX,myY-1,myX+myW,myY+9);
			if (myW==160) Render.src.set(0,149,160,159);
			else Render.src.set(85,138,160,148);
			Render.drawBitmap(image);
		} else {
			Render.setAlpha(90);
			Render.dest.set(myX,myY-1,myX+myW,myY+9);
			if (myW==160) Render.src.set(0,149,160,159);
			else Render.src.set(85,138,160,148);
			Render.drawBitmap(image);
		}
		Render.setAlpha(255);
	}

	
	// render thin menu option bar, and handle touch+mouse hovering
	public final static void renderMenuOptionThin(Texture image,String title, int myX, int mY) {
		renderMenuBar(image, myX,mY, 160, (menuSelectedItem==menuSelectedItem2) );
		GUI.renderText(title, 0, myX+4, mY, Render.width, 1, 0);
	}

	
	// render "main menu" option and handle touch+mouse select/hover
	public final static void renderMenuOption(Texture image, int myItemID, String title, int myX, int mY) {

		if (menuSelectedItem == myItemID) {
			Render.dest.set(myX - 2, mY - 2, myX + 76, mY + 14);
			Render.src.set(361, 166, 439, 182);
			Render.drawBitmap(image);
		} else {
			Render.dest.set(myX - 2, mY - 2, myX + 76, mY + 14);
			Render.src.set(361, 149, 439, 165);
			Render.drawBitmap(image);
		}

		Render.setAlpha(255);
		GUI.renderText(title, 0, myX + 2, mY + 2, Render.width, 1, 2);
	}
	
	
	// used for input-setup on PC
	public final static void renderMenuOptionInputSetup(Texture image, int myX, int mY, int keyIDX) {
		
		renderMenuBar(image,myX,mY, 75, (menuSelectedItem==menuSelectedItem2) );

		if (keyIDX<0) {
			GUI.renderText( "reset default", 0, myX+4, mY, Render.width, 1, 0);
		} else {
			GUI.renderText( GUI.keyboardConfigNames[keyIDX], 0, myX+4, mY, Render.width, 1, 0);
			GUI.renderButton(myX+55, mY, keyIDX , false);
		}
	}
	
	
	public final static void renderBar(int myX, int myY, int value, int maxvalue) {
		Render.setARGB(255,174,179,180);
		float percent=((62f / maxvalue) * value);
		GUI.renderText("<", 0, myX, myY, 16, 0, 0);
		Render.fillRect(myX + 6, myY, (int) percent, 5);
		GUI.renderText(">", 0, myX + 69, myY, 16, 0, 0);
	}
	
	
		
	
}
