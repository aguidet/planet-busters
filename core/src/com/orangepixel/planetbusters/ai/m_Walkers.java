package com.orangepixel.planetbusters.ai;

import com.orangepixel.planetbusters.Bullets;
import com.orangepixel.planetbusters.FX;
import com.orangepixel.planetbusters.Globals;
import com.orangepixel.planetbusters.Player;
import com.orangepixel.planetbusters.World;
import com.orangepixel.utils.Light;

/**
 * walker type monster entities (anything that just moves left <-> right in the level)
 * 
 * @author orangepascal / orangepixel.net
 *
 */
public class m_Walkers {

	public final static int		WORM = 0,
								ROBOEYE = 1;
	
	
	public final static int 	aiWormIdle = 0,
								aiWormSlide = 1,
								aiDroneIdle = 2,
								aiDroneAttack = 3;
	
	public final static int propXOffset = 0,
							propYOffset = 1,
							propWidth = 2,
							propHeight = 3;

	public final static int[][]	properties = new int[][] {
		{0,82,12,9},		// worm
		{32,21,10,10},		// roboeye
	};		
	
	/* 
	 * ================
	 * initialise provided monster into our type
	 * ================
	 */
	
	public final static void init(Monsters e, World myWorld) 
	{
		e.xOffset=properties[e.subType][propXOffset];
		e.yOffset=properties[e.subType][propYOffset];
		e.w=properties[e.subType][propWidth];
		e.h=properties[e.subType][propHeight];

		if (Globals.getRandom(100)>50)
		{
			e.myDirection=Globals.RIGHT;
		}
		else
		{
			e.myDirection=Globals.LEFT;
		}

		
		// for variation, pick one of two starting frames by default
		e.xOffset+=(Globals.getRandom(2)*e.w);
		
		switch (e.subType)
		{
			case WORM:
				e.y+=4;
				e.aiState=aiWormIdle;
				e.aiCountdown=Globals.getRandom(64);
				e.animationDelay=4;

				
				e.energy=4+(1*myWorld.difficulty);		
			break;
			
			case ROBOEYE:
				e.y+=3;
				e.aiState=aiDroneIdle;
				e.energy=2+(3*myWorld.difficulty);
			break;
				
		}
	}

	/*
	 * ================
	 * update specified monster every tick
	 * ================
	 * 	
	 */
	public final static void update(Monsters e, World myWorld, Player myPlayer, boolean hitPlayer) 
	{
		int tx;
		int ty;
		
		switch (e.aiState) {
			case aiWormIdle:
				if (e.aiCountdown<=8) e.xOffset=e.w;
				
				if (e.aiCountdown>0) 
				{
					e.aiCountdown--;
				}
				else 
				{
					e.aiState=aiWormSlide;
					if (e.myDirection==Globals.RIGHT) 
					{
						e.xSpeed=32;
					}
					else 
					{
						e.xSpeed=-32;
					}
					e.xOffset=properties[WORM][propXOffset];
				}
			break;
			
			case aiWormSlide:
				e.doHorizontal(myWorld);
				
				e.xSpeed=e.xSpeed>>1;
				
				if (e.xSpeed>-2 && e.xSpeed<2) 
				{
					e.xSpeed=0;
					e.aiState=aiWormIdle;
					e.aiCountdown=16;
				}
				
				
				ty=(e.y>>4);
				if (e.myDirection==Globals.LEFT) 
				{
					tx=(e.x>>4);
					if (myWorld.isSolid(tx, ty)) 
					{
						e.x=(tx<<4)-e.w;
						e.floatX=e.x<<4;
						e.xSpeed=0;
						e.aiState=aiWormIdle;
						e.aiCountdown=16;
						e.myDirection=Globals.RIGHT;
					}
				} 
				else 
				{
					tx=(e.x+e.w)>>4;
					if (myWorld.isSolid(tx, ty)) 
					{
						e.x=(tx<<4)+16;
						e.floatX=e.x<<4;
						e.xSpeed=0;
						
						e.aiState=aiWormIdle;
						e.aiCountdown=16;
						e.myDirection=Globals.LEFT;
					}							
				}
				
				if (hitPlayer) myPlayer.hit();
				Light.addLight(e.x-myWorld.worldOffsetX, e.y-myWorld.worldOffsetY, 16, Light.LightType_Sphere, 64,128,0,128);
			break;
			
			
			
			case aiDroneIdle:
				if (e.playerInOurSight(myPlayer)) {
					e.activated=true;
					
					e.targetX=e.x;
					if (e.x<myPlayer.x-24)
					{
						e.targetX=myPlayer.x-24;
					}
					else if (e.x>myPlayer.x+24)
					{
						e.targetX=myPlayer.x+24;
					}
					
					if (e.targetX>e.x) 
					{
						e.xSpeed=16;
						e.myDirection=Globals.RIGHT;
					} else if (e.targetX<e.x) 
					{
						e.xSpeed=-16;
						e.myDirection=Globals.LEFT;
					}
					e.aiState=aiDroneAttack;
				}
				
				if (e.aiCountdown>0)
				{
					e.aiCountdown--;
				}
				else 
				{
					if (e.myDirection==Globals.LEFT) 
					{
						e.myDirection=Globals.RIGHT;
					}
					else 
					{
						e.myDirection=Globals.LEFT;
					}
					e.aiCountdown=80;
				}
			break;
			
			
			case aiDroneAttack:
				e.doHorizontal(myWorld);
				

				if (e.xSpeed==0 || (e.x>e.targetX-4 && e.x<e.targetX+4)) 
				{
					e.xSpeed=0;
					e.aiState=aiDroneIdle;
					e.aiCountdown=32;
				}
				
				if (e.animationDelay>0)
				{
					e.animationDelay--;
				}
				else 
				{
					e.animationDelay=8;
					if (e.xOffset==properties[ROBOEYE][propXOffset]) 
					{
						e.xOffset=properties[ROBOEYE][propXOffset]+e.w;
					}
					else 
					{
						e.xOffset=properties[ROBOEYE][propXOffset];
					}
				}
				
				if (e.fireDelay>0) 
				{
					e.fireDelay--;
				}
				else 
				{
					e.fireDelay=8;
					Bullets.addBullets(Bullets.bDEFAULTBULLET, e.x, e.y+4, 0, e.myDirection, Bullets.bOWNER_MONSTER, myWorld);
				}
				
				
				if (hitPlayer) myPlayer.hit();
			break;
		}
				
	
		if (e.subType==ROBOEYE)
		{
			if (e.myDirection==Globals.RIGHT) 
			{
				e.yOffset=properties[ROBOEYE][propYOffset];
				Light.addLight(e.x+8-myWorld.worldOffsetX, e.y+3-myWorld.worldOffsetY, 16, Light.LightType_Sphere, 255,0,0,128);
			} 
			else 
			{
				e.yOffset=properties[ROBOEYE][propYOffset]+e.h;
				Light.addLight(e.x+1-myWorld.worldOffsetX, e.y+3-myWorld.worldOffsetY, 16, Light.LightType_Sphere, 255,0,0,128);
			}			

			if (e.aiState==aiDroneIdle) {
				// idle animation
				if (e.animationDelay>0) 
				{
					e.animationDelay--;
				} 
				else 
				{
					e.animationDelay=8;
					if (e.xOffset==properties[ROBOEYE][propXOffset])
					{
						e.xOffset=properties[ROBOEYE][propXOffset]+e.w;
					}
					else
					{
						e.xOffset=properties[ROBOEYE][propXOffset];
					}
				}
			}
		}
	
	

	}
	
	/*
	 * ================
	 * handle being hit by the specified bullet
	 * ================
	 */
	public final static boolean hit(Monsters e,Bullets myBullet, World myWorld) 
	{
		e.energy-=myBullet.energy;
		e.xSpeed=myBullet.myDirection<<4;

		// shortcircuit our shooting
		e.fireDelay=8;

		if (e.energy<=0) 
		{
			e.died=true;
			
			Monsters.addMonster(Monsters.mPICKUP, e.x, e.y, m_Pickup.CREDITS, myWorld);
			FX.addFX(FX.fEXPLOSION, e.x, e.y, 0, myWorld);
			
			for (int i=4; --i>=0;) 
			{
				FX.addFX(FX.fDEBRI, e.x+Globals.getRandom(3), e.y, 0, myWorld);
			}
		}

		return true;
	}

	/*
	 * ================
	 *  called after map generation is done, so some objects can turn 
	 *  them selves into "solid" wall type objects before the player starts
	 * ================
	 */
	public final static void solidify(Monsters e,World myWorld) 
	{
	}
	
	
}
