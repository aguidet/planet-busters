package com.orangepixel.planetbusters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;


/**
 * Keeps track of a player profile and all the data that corresponds
 * to such a profile
 * @author orangepascal / orangepixel.net
 *
 */
public class PlayerProfile {
	
	String				fileName;

	// audio?
	boolean 			useMusic;
	boolean				useSFX;
	boolean				useFullscreen;
	int					storedWindowedModeID;
	int					musicVolume;
	int					soundVolume;
	

	// input settings (touch,gamepad,keyboard)
	int[]				stickX;
	int[]				stickY;
	int[]				keyboardSettings;
	int[]				controller1;
	int[]				controller2;

	// simple check for cheating
	boolean				CRCcheckPassed;
	

	
	

	/* ===============
	 * initialise the profile object providing the filename to use
	 * ===============
	 */
	public PlayerProfile(String myFile) {
		fileName=myFile;
		
		stickX=new int[6];
		stickY=new int[6];
		
		keyboardSettings=new int[16];
		

		// signal a reset is needed
		stickX[0]=-999;
		stickY[0]=-999;

		controller1=new int[12];
		controller2=new int[12];
		
		musicVolume=4;
		soundVolume=7;
	}
	
	

	public final void loadSettings() {
		int myCRC=0;
		
		Preferences prefs = Gdx.app.getPreferences(fileName);

		// get profile preferences and settings
		useMusic=prefs.getBoolean("usemusic", true);
		useSFX=prefs.getBoolean("usesfx",true);
		useFullscreen=prefs.getBoolean("useFullscreen",false);
		storedWindowedModeID=prefs.getInteger("storedWindowedModeID",2); //	1080x720 by default -1);
		
		musicVolume=prefs.getInteger("musicvolume",3);
		soundVolume=prefs.getInteger("soundvolume",7);
		
		

		// load our controller presets (gamepad buttons)
		for (int i=12; --i>=0;) 
		{
			controller1[i]=prefs.getInteger("controller1"+i,-999);
			
			if (controller1[i]!=-999)
			{
				controller2[i]=prefs.getInteger("controller2"+i,controller1[i]);
			}
			else 
			{
				controller2[i]=prefs.getInteger("controller2"+i,-999);
			}
		}
		
		// offsets for touch-screen thumb sticks
		for (int i=6; --i>=0;) 
		{
			stickX[i]=prefs.getInteger("stickx"+i,-999);
			stickY[i]=prefs.getInteger("sticky"+i,-999);
		}
		
		// and of course our keyboard settings
		for (int i=16; --i>=0;) 
		{
			keyboardSettings[i]=prefs.getInteger("keyboardSettings"+i,-1);
		}
		
		// simple way to do checking on cheating
		// the total of a select couple of values from the preferences
		// are stored in the prefs under a unsuspecting name
		// and must be the same between saving and loading, 
		// if not.. the settings have been changed for possibly cheating (unlocks, progress, etc)
		// 
		int CRCcheck=prefs.getInteger("LastMinuteEvent", -1);
		
		CRCcheckPassed=false;
		if (CRCcheck>-1) 
		{
			if (CRCcheck==myCRC)
			{
				CRCcheckPassed=true;
			}
			else 
			{
				CRCcheckPassed=false;
			}
		} else {
			CRCcheckPassed=true;
		}
		
		
		if (CRCcheckPassed)
		{
			Globals.debug("CRC valid");
		}
		else 
		{
			Globals.debug("CRC invalid");
		}
	}

	
	
	public final void saveSettings() {
		int myCRC=0;
		
		Preferences prefsEditor = Gdx.app.getPreferences(fileName);
		
		// save settings and preferences
		prefsEditor.putBoolean("usemusic", useMusic);
		prefsEditor.putBoolean("usesfx", useSFX);
		prefsEditor.putBoolean("useFullscreen",useFullscreen);
		prefsEditor.putInteger("storedWindowedModeID",storedWindowedModeID);
		
		prefsEditor.putInteger("musicvolume",musicVolume);
		prefsEditor.putInteger("soundvolume",soundVolume);


		for (int i=6; --i>=0;) 
		{
			prefsEditor.putInteger("stickx"+i,stickX[i]);
			prefsEditor.putInteger("sticky"+i,stickY[i]);
		}		
		
		
		for (int i=16; --i>=0;) 
		{
			prefsEditor.putInteger("keyboardSettings"+i,keyboardSettings[i]);
		}
		
		
		for (int i=12; --i>=0;) 
		{
			prefsEditor.putInteger("controller1"+i,controller1[i]);
			prefsEditor.putInteger("controller2"+i,controller2[i]);
		}
		
		prefsEditor.putInteger("LastMinuteEvent", myCRC);
		
		prefsEditor.flush();
		
		prefsEditor=null;
	}

	
	
	
	/* ===============
	 * reset the touch-screen control offsets
	 * ===============
	 */
	public final void resetControls() {
		
		// left
		stickX[0]=4;
		stickY[0]=Render.height-36;

		// right
		stickX[1]=46;
		stickY[1]=Render.height-36;

		// render jump
		stickX[2]=Render.width-40;
		stickY[2]=Render.height-36;

		// render shoot
		stickX[3]=Render.width-82;
		stickY[3]=Render.height-36;
	}

	
	
	
	
	
}
