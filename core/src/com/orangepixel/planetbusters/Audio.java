package com.orangepixel.planetbusters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Audio handles the intialisation , playing and stopping of sound effects and music
 * 
 * @author orangepascal / orangepixel.net
 *
 */
public class Audio {

	public static boolean useMusic;
	public static boolean useSFX;
	public static int SoundVolume;
	public static int MusicVolume;
	
	public static Music myGameMusic;
	
	// sound effects
	public static Sound 	FX_SPLASH;
	
	
	/* ==============
	 * initialise the sound effects, call this on startup / init of the game
	 * ==============
	 */
	public final static void initSounds() {
		String root="audio/";

		FX_SPLASH=Gdx.audio.newSound(Gdx.files.internal(root + "fxsplash.mp3"));
	}
	
	
	
	/* ==============
	 * play specified sound effect
	 * ==============
	 */
	public final static long playSound(Sound sound) {
		long soundID=-1;
		if (useSFX) 
		{
			soundID=sound.play(SoundVolume / 10.0f);
		}
		return soundID;
	}


	/* ==============
	 * play specified sound , randomly changes pitch slightly, to vary often used sound effects
	 * ============== 
	 */
	public final static long playSoundPitched(Sound sound) {
		long soundID=-1;
		
		if (useSFX) 
		{
			soundID=sound.play(SoundVolume / 10.0f);
			sound.setPitch(soundID, (88+Globals.getRandom(16))/100.0f);
		}

		return soundID;
	}
	
	
	/* ==============
	 * stop all playing sounds (music and ambience mostly)
	 * ==============
	 */
	public final static void stopAllSounds() {
		if (myGameMusic != null) myGameMusic.stop();
	}

	/* ==============
	 * start playing the background music
	 * ==============
	 */
	public final static void playBackgroundMusic() {
		if (useMusic && myGameMusic != null) 
		{
			myGameMusic.play();
			myGameMusic.setVolume(MusicVolume / 10.0f);
		}
	}

	
	/* ==============
	 * stop playing background music
	 * ==============
	 */
	public final static void stopBackgroundMusic() {
		if (useMusic) 
		{
			if (myGameMusic != null && myGameMusic.isPlaying())
				myGameMusic.pause();
		}
	}	
	
}
