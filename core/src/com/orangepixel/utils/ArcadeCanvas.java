package com.orangepixel.utils;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.orangepixel.controller.GameInput;
import com.orangepixel.planetbusters.Render;
import com.orangepixel.social.Social;

public class ArcadeCanvas implements ApplicationListener {

	public static final boolean IS_MAC = System.getProperty("os.name").toLowerCase().contains("mac");
	public static final boolean IS_WINDOWS = System.getProperty("os.name").toLowerCase().contains("windows");
	public static final boolean IS_LINUX = System.getProperty("os.name").toLowerCase().contains("linux");
	
	public boolean argument_noController=false;
	public boolean argument_forceWindowed=false;
	
	public boolean isDesktop=false;
	public boolean isAndroid=false;
	public boolean isIOS = false;
	public static boolean isAndroidTV = false;

	public boolean isFullScreen=false;
	
	
	public final static int	ININIT = 1, 			
							INSPLASH = 2,											
							INMENU = 3,
							INITMAP = 19,
							INGAME = 20;
		

	// Collection of sprites (woohoo!)
	public static Texture[] sprites; // array of sprites
	// buffers for rendering the light
	FrameBuffer lightBuffer;
	TextureRegion lightBufferRegion;

	
	public int lastKeyCode;
	public boolean lastKeyLocked;
	
	
	
	
	// Social - platform specific (if null, nothing is done) (steam, google play, gamecenter, etc)
	public Social mySocial=null;

	
	// global variables
	public int GameState;
	public int worldTicks;
	public boolean secondPassed;
	public boolean paused;
	
	// use a global so we can do Render.setAlpha() and all render calls will use it
	public static int globalAlpha;
	public static int globalRed;
	public static int globalGreen;
	public static int globalBlue;	
	

		
	
	public boolean						switchFullScreen;
	public boolean						switchFullScreenLocked;
	
	public boolean						triggerFullScreenSwitch=false;

	// Check if keyboard is being used (so you can hide the onscreen controls if you like)
	public boolean						keyBoardOut=true;

	
	// system variables (framerate, mouse handling ,etc)
	public int myFramerate;
	private int fpsCount;
	private long loopEnd;
	private long loopPause;
	private static int displayW;
	private static int displayH;
		
	
	// rendering buffers
	FrameBuffer m_fbo;
	TextureRegion m_fboRegion=null;
	
	FrameBuffer m_fboTop;
	TextureRegion m_fboTopRegion=null;
	
	
	
	
	
	@Override
	public void create() {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		isDesktop=false;
		isAndroid=false;
		isIOS=false;

		switch (Gdx.app.getType()) {
			case Android:
				isAndroid=true;
				GameInput.IS_ANDROID=true;
				GameInput.controller1.isTouchscreen();
			break;
			
			case iOS:
				isIOS=true;
				GameInput.IS_IOS=true;
				GameInput.controller1.isTouchscreen();
			break;
				
			
			default:
				isDesktop=true;
			break;
		}

		displayW=(int)w;
		displayH=(int)h;


		// find height closest to 160, and matching width
		Render.height=(int)(displayH/Math.floor(displayH/160));
		Render.width=(int)(displayW/(displayH/ (displayH/Math.floor(displayH/160)) ));
		Render.fullScreenWidth=displayW;
		Render.fullScreenHeight=displayH;
		
		Render.initRender();
		
		
		// we use less, but make sure we have enough slots
		sprites=new Texture[32];

		
		
		
		loopPause = System.currentTimeMillis();
		GameState=ININIT;

		
		
		Gdx.app.getInput().setInputProcessor(GameInput.myProcessor);
		
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);

		if (!argument_noController)  GameInput.initControllers();
	}
	
	
	

	@Override
	public void dispose() {
		if (mySocial!=null) mySocial.disposeSocial();
		Render.batch.dispose();
	}

	
	

	public static int PowerOf2(int n) {
		  int k=1;
		  while (k<n) k*=2;
		  return k;
	}	

	
	
	
	@Override
	public void resize(int width, int height) {
		displayW=width;
		displayH=height;


		Render.height=(int)(displayH/Math.floor(displayH/160));
		Render.width=(int)(displayW/(displayH/ (displayH/Math.floor(displayH/160)) ));
		
		
		Gdx.app.log("opdebug","Display:"+displayW+"x"+displayH+"  -  pixels:"+Render.width+"x"+Render.height);
		
		if (m_fbo!=null) m_fbo.dispose();
	    m_fbo = new FrameBuffer(Format.RGB888, PowerOf2(Render.width), PowerOf2(Render.height), false);
	    m_fbo.getColorBufferTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	    m_fboRegion = new TextureRegion(m_fbo.getColorBufferTexture(),0,m_fbo.getHeight()-Render.height,Render.width,Render.height);
//	    m_fboRegion = new TextureRegion(m_fbo.getColorBufferTexture(),0,0,m_fbo.getWidth(),m_fbo.getHeight());
	    m_fboRegion.flip(false, false);

		if (m_fboTop!=null) m_fboTop.dispose();
		m_fboTop = new FrameBuffer(Format.RGBA8888, PowerOf2(Render.width), PowerOf2(Render.height), false);
		m_fboTop.getColorBufferTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		m_fboTopRegion = new TextureRegion(m_fboTop.getColorBufferTexture(),0,m_fboTop.getHeight()-Render.height,Render.width,Render.height);
		m_fboTopRegion.flip(false, false);

	    
	    
	    // FakedLight
	    if (lightBuffer!=null) lightBuffer.dispose();
	    lightBuffer = new FrameBuffer(Format.RGBA8888, PowerOf2(Render.width), PowerOf2(Render.height), false);
	    lightBuffer.getColorBufferTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	    lightBufferRegion = new TextureRegion(lightBuffer.getColorBufferTexture(),0,lightBuffer.getHeight()-Render.height,Render.width,Render.height);
	    lightBufferRegion.flip(false, false);
	    
	    

		Render.camera.setToOrtho(true, Render.width,Render.height);
	    
		
		// trigger a reinit of the controllers
	    if (GameInput.controllersFound>0) Controllers.clearListeners();
	    GameInput.controllersFound=0;
	}


	
	
	
	// openGL Render() - 60 frames per second
	@Override
	public void render() {
		
		if (Gdx.input.isKeyPressed(Keys.ENTER) && 
				(Gdx.input.isKeyPressed(Keys.ALT_LEFT) || Gdx.input.isKeyPressed(Keys.ALT_RIGHT)) )  {
			if (!switchFullScreenLocked) {
				switchFullScreen=true;
				switchFullScreenLocked=true;
				triggerFullScreenSwitch=true;
			}
		} else {
			switchFullScreen=false;
			switchFullScreenLocked=false;
		}		
		
		
		if (triggerFullScreenSwitch) {
			if (!GameInput.controller1.isMouse || (!GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)) {
				setDisplayMode(1080, 720, !isFullScreen);
				triggerFullScreenSwitch=false;
			}
		}
		
		if (!GameInput.controller1.isMouse) {
			if (isFullScreen) Gdx.input.setCursorCatched(true);
		}
		
		
//		loopStart = System.currentTimeMillis();

    	worldTicks++;																// global worldticking
		if (worldTicks>1000) worldTicks=0;											// heart-beat of the game
		
		// transform touch location to pixel-art screen locations
		GameInput.touchX = -1;
		GameInput.touchY = -1;
		if (GameInput.mTouchX[0] >= 0 && GameInput.mTouchY[0] >= 0) {
			GameInput.touchX=GameInput.mTouchX[0];
			GameInput.touchY=GameInput.mTouchY[0];
		}
	        
			

		// render to pixel-art framebuffer
        m_fbo.begin();		
        	Gdx.gl.glDisable(GL20.GL_BLEND);
        	Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        	Render.camera.setToOrtho(true, m_fbo.getWidth(), m_fbo.getHeight());
        	Render.camera.update();
	        
        	Render.batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        	Render.setAlpha(255);
        	
        	// gamelogic + render
			GameLoop();
			
			if (Render.globalTexture!=null) {
				Render.batch.end();
				Render.globalTexture=null;
			}
		m_fbo.end();

			
		// use FakeLight system?
		if (Light.isLightRendering) {
        		
        	// FakedLights
        	lightBuffer.begin();
	        Gdx.gl.glClearColor(Light.ambientColor.r,Light.ambientColor.g,Light.ambientColor.b,Light.ambientColor.a);
	        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

	        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
	        Gdx.gl.glEnable(GL20.GL_BLEND);
    
	        Render.batch.begin();
			Render.batch.setBlendFunction(GL20.GL_SRC_ALPHA, -1);

	        
			for (int i=0; i<Light.myLights.length; i++) {
				if (Light.myLights[i].isActive()) {
					Render.batch.setColor(Light.myLights[i].getColor());
					
					float tx=Light.myLights[i].getX();
					float ty=Light.myLights[i].getY();
					float tw=(128/100f)*Light.myLights[i].getDistance();
					
					// 64,92
					
					switch (Light.myLights[i].getLightType()) {
						case Light.LightType_Up:
							tx-=((128/100f)*Light.myLights[i].getDistance())/2;
							ty-=((92/100f)*Light.myLights[i].getDistance());
							Render.batch.draw(Light.lightSprite, tx,ty,
									tw,tw,
									0,128, 
									128,128,
									false,true);
						break;

						case Light.LightType_Right:
							tx-=((128/100f)*Light.myLights[i].getDistance())/2;
							ty-=((128/100f)*Light.myLights[i].getDistance());
							Render.batch.draw(Light.lightSprite,tx,ty,  
									tw/2,tw,
									tw,tw,
									1f,1f,
									90, 
									0,128,
									128,128,
									false,true);
						break;
						

						case Light.LightType_Down:
							tx-=((128/100f)*Light.myLights[i].getDistance())/2;
							ty+=((128/100f)*Light.myLights[i].getDistance());
							Render.batch.draw(Light.lightSprite,tx,ty,  
									tw/2,0,
									tw,tw,
									1f,1f,
									180, 
									0,128,
									128,128,
									false,true);
						break;			
						

						case Light.LightType_Left:
							tx-=((128/100f)*Light.myLights[i].getDistance())/2;
							ty-=((128/100f)*Light.myLights[i].getDistance());
							Render.batch.draw(Light.lightSprite,tx,ty,  
									tw/2,tw,
									tw,tw,
									1f,1f,
									270, 
									0,128,
									128,128,
									false,true);
						break;							

						
						case Light.LightType_SphereTense:
							tx-=(tw/2);
							ty-=(tw/2);
							Render.batch.draw(Light.lightSprite, tx,ty,
									tw,tw,
									256,0, 
									128,128,
									false,true);
						break;
						
						case Light.LightType_FLARE:
							tx-=(tw/2);
							ty-=(tw/2);
							Render.batch.draw(Light.lightSprite, tx,ty,
									tw,tw,
									128,128, 
									128,128,
									false,true);
						break;
						
						default:
							tx-=(tw/2);
							ty-=(tw/2);
							Render.batch.draw(Light.lightSprite, tx,ty,
									tw,tw,
									0,0, 
									128,128,
									false,true);
						break;
					}
					
				}
//					FLight.myLights[i].setActive(false);
			}

					
	        Render.batch.end();
	        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE);

	        lightBuffer.end();
        }		
			

		 // render pixel-art buffer to screen
		 if(m_fbo != null) {
			Render.camera.setToOrtho(true, displayW, displayH);
			Render.camera.update();
			
			Render.batch.begin();        
			Render.batch.setProjectionMatrix(Render.camera.combined);
			Render.batch.disableBlending(); //setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
			Render.batch.setColor(1,1,1,1);
			Render.batch.draw(m_fboRegion, 0, 0, displayW, displayH);
			Render.batch.end();
			Render.batch.enableBlending();
		 }


		 if (Light.isLightRendering) {
			Render.batch.begin();
	        Render.batch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
			Render.batch.draw(lightBufferRegion, 0, 0,displayW,displayH);               
			Render.batch.end();
	        Render.batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		 }
			 
    	// render "post light" stuff (statusbar, pause screen, etc)
		 
		 m_fboTop.begin();		
			 Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
			 Gdx.gl.glClear(GL20. GL_COLOR_BUFFER_BIT);
			 Render.camera.setToOrtho(true, m_fboTop.getWidth(), m_fboTop.getHeight());
			 Render.camera.update();

			 GameLoopPostLights();
        
			if (Render.globalTexture!=null) {
				Render.batch.end();
				Render.globalTexture=null;
			}
	
		m_fboTop.end();

		
//		Gdx.gl.glBlendFunc(GL20.GL_DST_COLOR, -1); //GL20.GL_ZERO);
			
		// render framebuffer to fullscreen
		Gdx.gl.glDisable(GL20.GL_BLEND);
		Render.camera.setToOrtho(true, displayW, displayH);
		Render.camera.update();

		Render.batch.begin();
		Render.batch.setProjectionMatrix(Render.camera.combined);
		Render.batch.setColor(1,1,1,1);
		Render.batch.draw(m_fboTopRegion, 0, 0, displayW, displayH);               
		Render.batch.end();

		 
		 
		fpsCount++;
		if (loopEnd-loopPause>1000) {
			secondPassed=true;
			loopPause=loopEnd;
			if (GameInput.controllersFound==0) GameInput.initControllers();
			myFramerate=fpsCount;
			fpsCount=0;
		}
		

		if (mySocial!=null) mySocial.processInfo();

		loopEnd = System.currentTimeMillis();

	}
	

	
	
	public DisplayMode[] modes; 
	public int currentModeID=-1;
	private int fallbackModeID;
	
	public void setDisplayMode(int width, int height, boolean fullscreen) {
		
		if (argument_forceWindowed) fullscreen=false;
		
		// return if requested DisplayMode is already set
        if ((Gdx.app.getGraphics().getWidth() == width) && 
			(Gdx.app.getGraphics().getHeight() == height) && 
			(Gdx.app.getGraphics().isFullscreen() == fullscreen)) {
			return;
		}
		
        if (fullscreen) {
        	// just use current desktop display width*height so we know it's fullscreen correctly
        	width=Gdx.app.getGraphics().getDesktopDisplayMode().width;
        	height=Gdx.app.getGraphics().getDesktopDisplayMode().height;
        }

        
		DisplayMode targetDisplayMode = null;
		DisplayMode fallBackMode = null;

		float 	baseRatio=(float)width/(float)height;	// 1,5
		float	currentRatio;
		
		
		modes = Gdx.app.getGraphics().getDisplayModes();
		
		if (fullscreen) {
			int freq = 0;
			
			for (int i=0;i<modes.length;i++) {
				DisplayMode current = modes[i];
				currentRatio=(float)current.width/(float)current.height;
				
				if (current.width>=width && current.height>=height && currentRatio>=baseRatio) {
					if (current.refreshRate >= freq && current.bitsPerPixel >= Gdx.app.getGraphics().getDesktopDisplayMode().bitsPerPixel) {
						targetDisplayMode = current;
						currentModeID=i;
						freq = targetDisplayMode.refreshRate;
					}
				} else {
					// find a "best" resolution if we can't find an exact math on ratio/w/h/bbp
					if ((current.bitsPerPixel == Gdx.app.getGraphics().getDesktopDisplayMode().bitsPerPixel ) &&
					    (current.refreshRate == Gdx.app.getGraphics().getDesktopDisplayMode().refreshRate )) {
						fallBackMode = current;
						fallbackModeID=i;
					}
				}
			}
			
			if (targetDisplayMode!=null) {
				Gdx.app.getGraphics().setDisplayMode(targetDisplayMode.width, targetDisplayMode.height,true);
//				Gdx.app.log("opdebug","displaymode:"+targetDisplayMode.width+"x"+targetDisplayMode.height);
			} else if (fallBackMode!=null) {
				Gdx.app.getGraphics().setDisplayMode(fallBackMode.width, fallBackMode.height,true);
				currentModeID=fallbackModeID;
			}
		} else {
			Gdx.app.getGraphics().setDisplayMode(width,height, false);
		}

		
		isFullScreen=fullscreen;
		
		if (isFullScreen) {
			Gdx.input.setCursorPosition((displayW>>1),(displayH>>1));
			Gdx.input.setCursorCatched(true);
		} else {
			Gdx.input.setCursorPosition((displayW>>1),(displayH>>1));
			Gdx.input.setCursorCatched(false);
		}
		
	
		// trigger a reinit of the controllers
		GameInput.controllersFound=0;
	}	
	
	
	
	
	// FakeLight system
	public final void setAmbientLight(float red, float green, float blue, float alpha) {
		Light.ambientColor.set(red,green,blue,alpha/4); 
	}
	

	

	
	
	
	
	
	
	public void pause() {}
	public void resume() {}	
	public void engineInit(){};
	public void GameLoop(){};
	public void GameLoopPostLights(){};

	
}
;