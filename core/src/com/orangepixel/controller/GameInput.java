package com.orangepixel.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import com.orangepixel.planetbusters.GUI;
import com.orangepixel.planetbusters.Render;

public class GameInput {
	
	public static final boolean IS_MAC = System.getProperty("os.name").toLowerCase().contains("mac");
	public static final boolean IS_WINDOWS = System.getProperty("os.name").toLowerCase().contains("windows");
	public static final boolean IS_LINUX = System.getProperty("os.name").toLowerCase().contains("linux");
	
	// set these from your ApplicationListener.create()
	public static boolean IS_ANDROID = false;
	public static boolean IS_IOS = false;
	
	
	// allown unidentifed controllers? (if true, they will default to xbox mapping, false they will be ignored) 
	public static boolean		controllersAllowUnknown=true;	
	
	
	// setup optional controllers (keyboard is also a controller)
	public static Gamepad		controller1=new Gamepad();
	public static Gamepad		controller2=new Gamepad();
	public static int			controllersFound=0;
	
	  
	// Touch screen related (only one of those attached at a time)
	public static boolean touchReleased=true;
	public static float touchX=-1;
	public static float touchY=-1;
	public static int[] mTouchX = new int[6];
	public static int[] mTouchY = new int[6];
	
	// mouse related code (only one of those attached at a time)
	public static float 	cursorX=-1;
	public static float 	cursorY=-1;
	public static int 		hideMouse;
	
	// keyboard (only .. you get the point)
	public static int 		lastKeyCode;
	public static boolean 	lastKeyLocked;

	
	// keyboard + mouse listeners
	public static InputProcessor myProcessor = new InputProcessor() {
		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			if (pointer<5) {
				mTouchX[pointer]=-1;
				mTouchY[pointer]=-1;
				touchReleased=true;
			}
			
			if (IS_MAC || IS_LINUX || IS_WINDOWS) {
				controller1.isMouse();
				hideMouse=999;
				if (button==Input.Buttons.LEFT) {
					controller1.BUTTON_X=false;
					controller1.BUTTON_Xlocked=false;
				}
				
				if (button==Input.Buttons.RIGHT) {
					controller1.BUTTON_Y=false;
					controller1.BUTTON_Ylocked=false;
				}
				
				if (button==Input.Buttons.MIDDLE) {
					controller1.BUTTON_A=false;
					controller1.BUTTON_Alocked=false;
				}

			}
			return false;
		}
		
		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			controller1.isTouchscreen();
			
			if (pointer<5) {
				touchX = (Render.width / 100f) * ((100f / Render.fullScreenWidth) * screenX);
				touchY = (Render.height / 100f) * ((100f / Render.fullScreenHeight) * screenY);
				
				mTouchX[pointer]=(int)(touchX);
				mTouchY[pointer]=(int)(touchY);
			}
			
			if (IS_MAC || IS_LINUX || IS_WINDOWS) {
				controller1.isMouse();
				hideMouse=999;
				processMouse(screenX,screenY);
			}
			
			return false;
		}
		
		@Override
		public boolean touchDown(int screenX, int screenY, int pointer, int button) {
			controller1.isTouchscreen();
			
			if (pointer<5) {
				touchX = (Render.width / 100f) * ((100f / Render.fullScreenWidth) * screenX);
				touchY = (Render.height / 100f) * ((100f / Render.fullScreenHeight) * screenY);
				
				mTouchX[pointer]=(int)(touchX);
				mTouchY[pointer]=(int)(touchY);
			}
		
			if (IS_MAC || IS_LINUX || IS_WINDOWS) {
				controller1.isMouse();
				if (button==Input.Buttons.LEFT) {
					controller1.BUTTON_X=true;
				}

				// swapped these two with other games
				if (button==Input.Buttons.RIGHT) {
					controller1.BUTTON_Y=true;
				}
				
				if (button==Input.Buttons.MIDDLE) {
					controller1.BUTTON_A=true;
				}
				
				hideMouse=999;
				processMouse(screenX,screenY);
			}
			return false;
		}
		
		@Override
		public boolean scrolled(int amount) {
			controller1.isMouse();
			controller1.scrollWheelUp=false;
			controller1.scrollWheelDown=false;
			if (amount<0) controller1.scrollWheelUp=true;
			else if (amount>0) controller1.scrollWheelDown=true;
			return false;
		}
		
		@Override
		public boolean mouseMoved(int screenX, int screenY) {
			controller1.isMouse();
			Gdx.input.setCursorCatched(true);
			hideMouse=999;
			
			processMouse(screenX,screenY);
			return false;
		}
		
		@Override
		public boolean keyUp(int keycode) {
			lastKeyCode=-1;
			lastKeyLocked=false;

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_left]) {
				controller1.isKeyboard();
				controller1.leftPressed=false;
				controller1.leftLocked=false;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_right]) {
				controller1.isKeyboard();
				controller1.rightPressed=false;
				controller1.rightLocked=false;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_up]) {
				controller1.isKeyboard();
				controller1.upPressed=false;
				controller1.upLocked=false;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_down]) {
				controller1.isKeyboard();
				controller1.downPressed=false;
				controller1.downLocked=false;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_select]) {
				controller1.BUTTON_X=false;
				controller1.BUTTON_Xlocked=false;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_cancel]) {
				controller1.backPressed=false;
				controller1.backLocked=false;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_map]) {
				controller1.BUTTON_SPECIAL1=false;
				controller1.BUTTON_SPECIAL1locked=false;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_inventory]) {
				controller1.BUTTON_Y=false;
				controller1.BUTTON_Ylocked=false;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_weapon1]) {
				controller1.switchWeaponOne=false;
				controller1.switchWeaponOneLocked=false;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_weapon2]) {
				controller1.switchWeaponTwo=false;
				controller1.switchWeaponTwoLocked=false;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_weapon3]) {
				controller1.switchWeaponThree=false;
				controller1.switchWeaponThreeLocked=false;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_weapon4]) {
				controller1.switchWeaponFour=false;
				controller1.switchWeaponFourLocked=false;
				return true;
			}
			
			switch (keycode) {
				case Keys.BACK:
					controller1.backPressed=false;
					controller1.backLocked=false;
					return true;
			}
			
			
			return false;
		}
		
		@Override
		public boolean keyTyped(char character) {
			return false;
		}
		
		@Override
		public boolean keyDown(int keycode) {
			lastKeyCode=keycode;
			
		
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_left]) {
				controller1.isKeyboard();
				controller1.leftPressed=true;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_right]) {
				controller1.isKeyboard();
				controller1.rightPressed=true;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_up]) {
				controller1.isKeyboard();
				controller1.upPressed=true;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_down]) {
				controller1.isKeyboard();
				controller1.downPressed=true;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_select]) {
				controller1.BUTTON_X=true;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_cancel]) {
				controller1.backPressed=true;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_map]) {
				controller1.BUTTON_SPECIAL1=true;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_inventory]) {
				controller1.BUTTON_Y=true;
				return true;
			}
			
			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_weapon1]) {
				controller1.switchWeaponOne=true;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_weapon2]) {
				controller1.switchWeaponTwo=true;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_weapon3]) {
				controller1.switchWeaponThree=true;
				return true;
			}

			if (keycode==GUI.keyboardConfig[GUI.keyboardConfig_weapon4]) {
				controller1.switchWeaponFour=true;
				return true;
			}

			
			switch (keycode) {
				case Keys.BACK:
					controller1.backPressed=true;
					return true;
			}
			
			return false;
		}
	};

	// used for AndroidTV devices, where controller 1 might not be the gamepad used but a remote
	// we'll sneakily swap the devices around so the game won't notice
	private static int firstdevice;
	  
	public final static void initControllers() {
		ControllerMapping pad=null;
		
		// joystick support!
		controllersFound=0;
		controller1.hashcode=-1;
		firstdevice=-1;
	
		
		for(Controller controller: Controllers.getControllers()) {
			// detect controller if we can
			if (!controller.getName().isEmpty()) 
			{
				if (IS_ANDROID) 
				{
					pad=new GSGamePadAndroid();
				} 
				else if (controller.getName().toLowerCase().contains("xbox")
						|| controller.getName().toLowerCase().contains("x-box")
						|| controller.getName().toLowerCase().contains("gamepad f")) 
				{	// "gamepad fxxx" are logitech gamepads
				
					if (IS_MAC) pad=new XBoxOSX();
					else if (IS_LINUX) {
						if (controller.getName().toLowerCase().contains("wireless receiver")) {
							pad=new XBoxLinuxAlt();
						} else {
							pad=new XBoxLinux();
						}
					} 
					else 
					{
						pad=new XBox();
					}
					
				} else if (controller.getName().toLowerCase().contains("playstation")) {
					// PS3 on windows 8 is detected, but won't register
					pad=new PS3();
				} else if (controller.getName().contains("Wireless Controller")) {
					if (IS_MAC) pad=new PS4OSX();
					else pad=new PS4();
				} else if (controller.getName().contains("Pro Ex")) {
					pad=new ProExPowerA();
				} else if (!controller.getName().toLowerCase().contains("flight")) {
					Gdx.app.log("opdebug", "Unknown controller: "+controller.getName() + " hash:"+controller.hashCode());
					if (controllersAllowUnknown) {
						// made sure it's not a flight stick or anything
						// now use generic mapping, and hope it works (basically this is XBox mapping)
						pad=new ControllerMapping();
					}
				}
			}
			
			
			if (pad!=null) {

				if (controller1.hashcode<0) {
					controller1.hashcode=controller.hashCode();
					controller1.mapping=pad;
					controller1.name=controller.getName();
				} else {
					controller2.hashcode=controller.hashCode();
					controller2.mapping=pad;
					controller2.name=controller.getName();
				}
				
				controllersFound++;
				controller.addListener(new ControllerListener() {
					
					public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) { return false; }
					public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) { return false; }
					public boolean povMoved(Controller arg0, int arg1, PovDirection arg2) {
						if (arg0==null || controller1==null) return false;
						

						if (arg0.hashCode()==controller1.hashcode && controller1.mapping!=null) {
							if (firstdevice<0) firstdevice=1;
							
							if (arg2==controller1.mapping.DPAD_UP) {
								controller1.upPressed=true;
								controller1.DPAD_UP=true;
								controller1.BUTTON_DPADUp=true;
							} else if (controller1.DPAD_UP) {
								controller1.DPAD_UP=false;
								controller1.upPressed=false;
								controller1.upLocked=false;
								controller1.BUTTON_DPADUp=false;
								controller1.BUTTON_DPADUpLocked=false;
							}

							if (arg2==controller1.mapping.DPAD_DOWN) {
								controller1.DPAD_DOWN=true;
								controller1.downPressed=true;
								controller1.BUTTON_DPADDown=true;
							} else if (controller1.DPAD_DOWN) {
								controller1.DPAD_DOWN=false;
								controller1.downPressed=false;
								controller1.downLocked=false;
								controller1.BUTTON_DPADDown=false;
								controller1.BUTTON_DPADDownLocked=false;
							}

							if (arg2==controller1.mapping.DPAD_RIGHT) {
								controller1.DPAD_RIGHT=true;
								controller1.rightPressed=true;
								controller1.BUTTON_DPADRight=true;
							} else if (controller1.DPAD_RIGHT) {
								controller1.DPAD_RIGHT=false;
								controller1.rightPressed=false;
								controller1.rightLocked=false;
								controller1.BUTTON_DPADRight=false;
								controller1.BUTTON_DPADRightLocked=false;
							}
							
							if (arg2==controller1.mapping.DPAD_LEFT) {
								controller1.DPAD_LEFT=true;
								controller1.leftPressed=true;
								controller1.BUTTON_DPADLeft=true;
							} else if (controller1.DPAD_LEFT) {
								controller1.DPAD_LEFT=false;
								controller1.leftPressed=false;
								controller1.leftLocked=false;
								controller1.BUTTON_DPADLeft=false;
								controller1.BUTTON_DPADLeftLocked=false;
							}
							
						}
						
						
						
						if (controller2==null) return false;
						
						if (arg0.hashCode()==controller2.hashcode && controller2.mapping!=null) {
							
							if (arg2==controller2.mapping.DPAD_UP) {
								controller2.upPressed=true;
								controller2.DPAD_UP=true;
								controller2.BUTTON_DPADUp=true;
							} else if (controller2.DPAD_UP) {
								controller2.DPAD_UP=false;
								controller2.upPressed=false;
								controller2.upLocked=false;
								controller2.BUTTON_DPADUp=false;
								controller2.BUTTON_DPADUpLocked=false;
							}

							if (arg2==controller2.mapping.DPAD_DOWN) {
								controller2.DPAD_DOWN=true;
								controller2.downPressed=true;
								controller2.BUTTON_DPADDown=true;
							} else if (controller2.DPAD_DOWN) {
								controller2.DPAD_DOWN=false;
								controller2.downPressed=false;
								controller2.downLocked=false;
								controller2.BUTTON_DPADDown=false;
								controller2.BUTTON_DPADDownLocked=false;
							}

							if (arg2==controller2.mapping.DPAD_RIGHT) {
								controller2.DPAD_RIGHT=true;
								controller2.rightPressed=true;
								controller2.BUTTON_DPADRight=true;
							} else if (controller2.DPAD_RIGHT) {
								controller2.DPAD_RIGHT=false;
								controller2.rightPressed=false;
								controller2.rightLocked=false;
								controller2.BUTTON_DPADRight=false;
								controller2.BUTTON_DPADRightLocked=false;
							}
							
							if (arg2==controller2.mapping.DPAD_LEFT) {
								controller2.DPAD_LEFT=true;
								controller2.leftPressed=true;
								controller2.BUTTON_DPADLeft=true;
							} else if (controller2.DPAD_LEFT) {
								controller2.DPAD_LEFT=false;
								controller2.leftPressed=false;
								controller2.leftLocked=false;
								controller2.BUTTON_DPADLeft=false;
								controller2.BUTTON_DPADLeftLocked=false;
							}
							
							if (firstdevice<0 && IS_ANDROID) {
								// swap controllers around, so game just has to test controller1
								Gamepad controllertemp=controller1;
								controller1=controller2;
								controller2=controllertemp;
								firstdevice=1;
							}
							
							
						}						
						
						return false; 
						
					}
					public void disconnected(Controller arg0) {}
					public void connected(Controller arg0) {}
					public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) { return false; }
					
					public boolean buttonUp(Controller arg0, int arg1) {
//							last_button=-999;
//							pl2_last_button=-999;
						if (arg0==null || controller1==null) return false;
						
						if (arg0.hashCode()==controller1.hashcode && controller1.mapping!=null) {
							
							if (firstdevice<0) firstdevice=1;
							
//								Gdx.app.log("opdebug","buttonup:"+arg1);
							
							if (arg1==controller1.mapping.BUTTON_X) { // button_a) {
								controller1.BUTTON_X=false;
								controller1.BUTTON_Xlocked=false;
								return true;
							}
							if (arg1==controller1.mapping.BUTTON_A) { //button_b) {
								controller1.BUTTON_A=false;
								controller1.BUTTON_Alocked=false;
								return true;
							}
							if (arg1==controller1.mapping.BUTTON_B) { // button_c) {
								controller1.BUTTON_B=false;
								controller1.BUTTON_Blocked=false;
								controller1.backPressed=false;
								controller1.backLocked=false;
								return true;
							}	
							
							if (arg1==controller1.mapping.BUTTON_Y) {
								controller1.BUTTON_Y=false;
								controller1.BUTTON_Ylocked=false;
								return true;
							}
							
							if (arg1==controller1.mapping.BUTTON_LB) {
								controller1.BUTTON_LB=false;
								controller1.BUTTON_LBlocked=false;
								return true;
							}

							if (arg1==controller1.mapping.BUTTON_RB) {
								controller1.BUTTON_RB=false;
								controller1.BUTTON_RBlocked=false;
								return true;
							}
							if (arg1==controller1.mapping.BUTTON_START) { //button_b) {
								controller1.BUTTON_SPECIAL2=false;
								controller1.BUTTON_SPECIAL2locked=false;
								return true;
							}
							

							if (arg1==controller1.mapping.BUTTON_DPAD_UP) {
								controller1.upPressed=false;
								controller1.upLocked=false;
								controller1.BUTTON_DPADUp=false;
								controller1.BUTTON_DPADUpLocked=false;
								return true;
							}

							if (arg1==controller1.mapping.BUTTON_DPAD_DOWN) {
								controller1.downPressed=false;
								controller1.downLocked=false;
								controller1.BUTTON_DPADDown=false;
								controller1.BUTTON_DPADDownLocked=false;
								
								return true;
							}
							
							if (arg1==controller1.mapping.BUTTON_DPAD_RIGHT) {
								controller1.rightPressed=false;
								controller1.rightLocked=false;
								controller1.BUTTON_DPADRight=false;
								controller1.BUTTON_DPADRightLocked=false;
								return true;
							}
							
							if (arg1==controller1.mapping.BUTTON_DPAD_LEFT) {
								controller1.leftPressed=true;
								controller1.leftLocked=false;
								controller1.BUTTON_DPADLeft=false;
								controller1.BUTTON_DPADLeftLocked=false;
								return true;
							}				
									
						} else if (controller2.mapping!=null) {
							if (arg1==controller2.mapping.BUTTON_X) {
								controller2.BUTTON_X=false;
								controller2.BUTTON_Xlocked=false;
								return true;
							}
							if (arg1==controller2.mapping.BUTTON_A) {
								controller2.BUTTON_A=false;
								controller2.BUTTON_Alocked=false;
								return true;
							}
							if (arg1==controller2.mapping.BUTTON_START) { //button_b) {
								controller2.BUTTON_SPECIAL2=false;
								controller2.BUTTON_SPECIAL2locked=false;
								return true;
							}
							
							if (arg1==controller2.mapping.BUTTON_Y) {
								controller2.BUTTON_Y=false;
								controller2.BUTTON_Ylocked=false;
								return true;
							}
							if (arg1==controller2.mapping.BUTTON_B) {
								controller2.BUTTON_B=false;
								controller1.BUTTON_Blocked=false;
								controller2.backPressed=false;
								controller2.backLocked=false;
								return true;
							}			
							
							if (arg1==controller2.mapping.BUTTON_LB) {
								controller2.BUTTON_LB=false;
								controller2.BUTTON_LBlocked=false;
								return true;
							}

							if (arg1==controller2.mapping.BUTTON_RB) {
								controller2.BUTTON_RB=false;
								controller2.BUTTON_RBlocked=false;
								return true;
							}							

							if (arg1==controller2.mapping.BUTTON_DPAD_UP) {
								controller2.upPressed=false;
								controller2.upLocked=false;
								controller2.BUTTON_DPADUp=false;
								controller2.BUTTON_DPADUpLocked=false;
								return true;
							}

							if (arg1==controller2.mapping.BUTTON_DPAD_DOWN) {
								controller2.downPressed=false;
								controller2.downLocked=false;
								controller2.BUTTON_DPADDown=false;
								controller2.BUTTON_DPADDownLocked=false;
								return true;
							}
						
							if (arg1==controller2.mapping.BUTTON_DPAD_RIGHT) {
								controller2.rightPressed=false;
								controller2.rightLocked=false;
								controller2.BUTTON_DPADRight=false;
								controller2.BUTTON_DPADRightLocked=false;
								return true;
							}
							
							if (arg1==controller2.mapping.BUTTON_DPAD_LEFT) {
								controller2.leftPressed=true;
								controller2.leftLocked=false;
								controller2.BUTTON_DPADLeft=false;
								controller2.BUTTON_DPADLeftLocked=false;
								return true;
							}		

							if (firstdevice<0 && IS_ANDROID) {
								// swap controllers around, so game just has to test controller1
								Gamepad controllertemp=controller1;
								controller1=controller2;
								controller2=controllertemp;
								firstdevice=1;
							}							
						}
						return false;
					}
					
					@Override
					public boolean buttonDown(Controller arg0, int arg1) {
						if (arg0==null || controller1==null) return false;
						
						controller1.isGamepad();
						
						if (arg0.hashCode()==controller1.hashcode && controller1.mapping!=null) {
//								Gdx.app.log("opdebug","buttondown:"+arg1);

//								last_button=arg1;
							
							if (arg1==controller1.mapping.BUTTON_X) {
								controller1.BUTTON_X=true;
								return true;
							}
							if (arg1==controller1.mapping.BUTTON_A) {
								controller1.BUTTON_A=true;
								return true;
							}
							if (arg1==controller1.mapping.BUTTON_B) {
								controller1.BUTTON_B=true;
								controller1.backPressed=true;
								return true;
							}
							if (arg1==controller1.mapping.BUTTON_START) { //button_b) {
								controller1.BUTTON_SPECIAL2=true;
								return true;
							}
							
							
							if (arg1==controller1.mapping.BUTTON_Y) {
								controller1.BUTTON_Y=true;
								return true;
							}
							if (arg1==controller1.mapping.BUTTON_LB) {
								controller1.BUTTON_LB=true;
								return true;
							}
							if (arg1==controller1.mapping.BUTTON_RB) {
								controller1.BUTTON_RB=true;
								return true;
							}
							

							if (arg1==controller1.mapping.BUTTON_DPAD_UP) {
								controller1.upPressed=true;
								controller1.BUTTON_DPADUp=true;
								return true;
							}

							if (arg1==controller1.mapping.BUTTON_DPAD_DOWN) {
								controller1.downPressed=true;
								controller1.BUTTON_DPADDown=true;
								return true;
							}
							
							if (arg1==controller1.mapping.BUTTON_DPAD_RIGHT) {
								controller1.rightPressed=true;
								controller1.BUTTON_DPADRight=true;
								return true;
							}
							
							if (arg1==controller1.mapping.BUTTON_DPAD_LEFT) {
								controller1.leftPressed=true;
								controller1.BUTTON_DPADLeft=true;
								return true;
							}

						} else if (controller2.mapping!=null){
//								pl2_last_button=arg1;
							
							if (arg1==controller2.mapping.BUTTON_X) {
								controller2.BUTTON_X=true;
								return true;
							}
							if (arg1==controller2.mapping.BUTTON_A) {
								controller2.BUTTON_A=true;
								return true;
							}
							if (arg1==controller2.mapping.BUTTON_B) {
								controller2.BUTTON_B=true;
								controller2.backPressed=true;
								return true;
							}	
							if (arg1==controller2.mapping.BUTTON_Y) {
								controller2.BUTTON_Y=true;
								return true;
							}
							if (arg1==controller2.mapping.BUTTON_LB) {
								controller2.BUTTON_LB=true;
								return true;
							}
							if (arg1==controller2.mapping.BUTTON_RB) {
								controller2.BUTTON_RB=true;
								return true;
							}							
							
							if (arg1==controller2.mapping.BUTTON_DPAD_UP) {
								controller2.upPressed=true;
								controller2.BUTTON_DPADUp=true;
								return true;
							}

							if (arg1==controller2.mapping.BUTTON_DPAD_DOWN) {
								controller2.downPressed=true;
								controller2.BUTTON_DPADDown=true;
								return true;
							}
							
							if (arg1==controller2.mapping.BUTTON_DPAD_RIGHT) {
								controller2.rightPressed=true;
								controller2.BUTTON_DPADRight=true;
								return true;
							}
							
							if (arg1==controller2.mapping.BUTTON_DPAD_LEFT) {
								controller2.leftPressed=true;
								controller1.BUTTON_DPADLeft=true;
								return true;
							}							
						}
						
						return false;
					}
					
					@Override
					public boolean axisMoved(Controller arg0, int arg1, float arg2) {
						if (arg0==null || controller1==null) return false;
						
						
						
						arg2=arg2*128;

						if (arg0.hashCode()==controller1.hashcode && controller1.mapping!=null) {

							if (arg1==controller1.mapping.AXIS_LX) {
								if (arg2>-25 && arg2<25) {
									controller1.AXIS_LX=0;
								} else {
									controller1.AXIS_LX=(int)arg2;
									controller1.isGamepad();
								}
								
								
								if (controller1.mapping.reverseXAxis) controller1.AXIS_LX=-controller1.AXIS_LX;
								// also handle dpad stuff
								if (arg2<-64) controller1.leftPressed=true;
								else {
									controller1.leftPressed=false;
									controller1.leftLocked=false;
								}
								
								if (arg2>64) controller1.rightPressed=true;
								else {
									controller1.rightPressed=false;
									controller1.rightLocked=false;
								}
								
								
								return true;
								
							} else if (arg1==controller1.mapping.AXIS_LY) {
								if (arg2>-25 && arg2<25) controller1.AXIS_LY=0;
								else {
									controller1.AXIS_LY=(int)arg2;
									controller1.isGamepad();
								}
								
								if (controller1.mapping.reverseYAxis) controller1.AXIS_LY=-controller1.AXIS_LY;
								if (arg2<-64) controller1.upPressed=true;
								else {
									controller1.upPressed=false;
									controller1.upLocked=false;
								}
								
								
								if (arg2>64) controller1.downPressed=true;
								else {
									controller1.downPressed=false;
									controller1.downLocked=false;
								}
								
								return true;
							} else if (arg1==controller1.mapping.AXIS_RX) {
								if (arg2>-25 && arg2<25) controller1.AXIS_RX=0;
								else {
									controller1.AXIS_RX=(int)arg2;
									controller1.isGamepad();
								}
								if (controller1.mapping.reverseXAxis) controller1.AXIS_RX=-controller1.AXIS_RX;
								return true;
								
							} else if (arg1==controller1.mapping.AXIS_RY) {
								if (arg2>-25 && arg2<25) controller1.AXIS_RY=0;
								else {
									controller1.AXIS_RY=(int)arg2;
									controller1.isGamepad();
								}
								
								if (controller1.mapping.reverseYAxis) controller1.AXIS_RX=-controller1.AXIS_RX;
								return true;
							}
						} else if (controller2.mapping!=null) {
							// controller 2
							if (arg1==controller2.mapping.AXIS_LX) {
								if (arg2>-25 && arg2<25) controller2.AXIS_LX=0;
								else {
									controller2.AXIS_LX=(int)arg2;
									controller2.isGamepad();
								}
								if (controller2.mapping.reverseXAxis) controller2.AXIS_LX=-controller2.AXIS_LX; 
								
								if (arg2<-64) controller2.leftPressed=true;
								else {
									controller2.leftPressed=false;
									controller2.leftLocked=false;
								}
								
								if (arg2>64) controller2.rightPressed=true;
								else {
									controller2.rightPressed=false;
									controller2.rightLocked=false;
								}
								
								return true;
								
							} else if (arg1==controller2.mapping.AXIS_LY) {
								if (arg2>-25 && arg2<25) controller2.AXIS_LY=0;
								else {
									controller2.AXIS_LY=(int)arg2;
									controller2.isGamepad();
								}
								if (controller2.mapping.reverseYAxis) controller2.AXIS_LY=-controller2.AXIS_LY;

								if (arg2<-64) controller2.upPressed=true;
								else {
									controller2.upPressed=false;
									controller2.upLocked=false;
								}
								
								if (arg2>64) controller2.downPressed=true;
								else {
									controller2.downPressed=false;
									controller2.downLocked=false;
								}
								return true;
							} else if (arg1==controller2.mapping.AXIS_RX) {
								if (arg2>-25 && arg2<25) controller2.AXIS_RX=0;
								else controller2.AXIS_RX=(int)arg2;
								
								if (controller2.mapping.reverseXAxis) controller2.AXIS_RX=-controller2.AXIS_RX;
								return true;
								
							} else if (arg1==controller2.mapping.AXIS_RY) {
								if (arg2>-25 && arg2<25) controller2.AXIS_RY=0;
								else controller2.AXIS_RY=(int)arg2;
								
								if (controller2.mapping.reverseYAxis) controller2.AXIS_RY=-controller2.AXIS_RY;
								return true;
							}
							
						}
						return false;
					}
					
				});
			}
		}
	}	  
	
	
	
	private final static void processMouse(int screenX, int screenY) {
		if (Gdx.input.isCursorCatched()) {
			if (screenX<1) screenX=1;
			if (screenY<1) screenY=1;
			
			if (screenX>Render.fullScreenWidth-2) screenX=Render.fullScreenWidth-2;
			if (screenY>Render.fullScreenHeight-2) screenY=Render.fullScreenHeight-2;
			
			Gdx.input.setCursorPosition(screenX, screenY);
		}
		
		if (screenX<1 || screenY<1 || screenX>Render.fullScreenWidth-2 || screenY>Render.fullScreenHeight-2) return;
		
		cursorX = (int) ((Render.width / 100f) * ((100f / Render.fullScreenWidth) * screenX))-4;
		cursorY= (int) ((Render.height / 100f) * ((100f / Render.fullScreenHeight) * screenY))-4;

		if (cursorX<1) cursorX=1;
		if (cursorY<1) cursorY=1;
	}	
		

}
